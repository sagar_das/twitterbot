import smtplib,ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import logging

port = 465
password = 'commercecx2018'


logging.basicConfig(filename='cxmarketbot.log', filemode='a', format='%(name)s - %(levelname)s - %(message)s')

#creating ssl context
context = ssl.create_default_context()


sender_email = 'cxmobileapps@gmail.com'
receiver_email = 'vinaytoomu@commercecx.com'

message = MIMEMultipart("alternative")
message["Subject"] = "Weekly update from Marketing bot"
message["From"] = sender_email
message["To"] = receiver_email

text = """\
Hi,
This is message from bot"""

part1 = MIMEText(text, "plain")
#message.attach(part1)

body = """\
    <html>
      <body>
        <p>Hi,<br>
           Please find below the weekly status report for the marketing bot<br>
           <a href="http://www.realpython.com">Real Python</a> 
           has many great tutorials.
        </p>
      </body>
    </html>
    """

def sendEmail(body):
    global port,password,context,message

    part2 = MIMEText(body,"html")
    message.attach(part2)
    with smtplib.SMTP_SSL("smtp.gmail.com",port,context=context) as server:
        server.login('cxmobileapps@gmail.com',password)
        server.sendmail(sender_email,receiver_email,message.as_string())






def sendAlertMail(exception_message):
    global port, password, context, message
    receiver_email = "sagardas@commercecx.com"
    body = """
        <html>
          <body>
            <p>Hi,<br>
               The below exception has occurred in the bot
            </p>
            <p><b>Exception Description: </b><br>"""+exception_message+""""
            </body>
        </html>
        """
    part2 = MIMEText(body, "html")
    message.attach(part2)
    with smtplib.SMTP_SSL("smtp.gmail.com", port, context=context) as server:
        server.login('cxmobileapps@gmail.com', password)
        server.sendmail(sender_email, receiver_email, message.as_string())


def sendWeeklyStatus(report):
    try:
        body = """\
        <html>
          <body>
            <p>Hi,<br>
               Please find below the weekly status report for the <b>marketing bot</b>.<br>
            </p>
            <p>
            <table style="width:100%">
              <tr>
                <td>Total Followers count</td>
                <td>95</d>
              </tr>
              <tr>
                <td>Number of people expressing interest</td> 
                <td>25</td> 
              </tr>
              <tr>
                 <td>Total number of Tweets</td>
                <td>350</td> 
              </tr>
            </table>
            </p>
            <p>
            <b>Statistics</b><br>
            The total number of followers have increased by <b>50%</b> since last week. <br>
            The top hash tags used by the business community for this week are <b> #AI, #Cognitive #Salesforce #Experience </b>
            
            </p>
          </body>
        </html>
        """

        global port, password, context, message
        message_part = MIMEText(body, "html")
        message.attach(message_part)
        with smtplib.SMTP_SSL("smtp.gmail.com", port, context=context) as server:
            server.login('cxmobileapps@gmail.com', password)
            server.sendmail(sender_email, receiver_email, message.as_string())
    except Exception as e: logging.exception('Error in sending weekly status update mail'+str(e))


def sendWeeklyStatusTemp(report):
    try:
        body = """\
        <html>
          <body>
            <p>Hi,<br>
               Please find attached the list of trending tweets collected by the <b>marketing bot</b>.<br>
            </p>
            <p>------This is an auto generated message by the bot. Please do not reply back to this email id------<p>
            <p>Thanks,<br>
            CX Market bot<p>
          </body>
        </html>
        """

        global port, password, context, message

        filename = "weekly_trending_tweets2019-04-12 18:04:35.255748.xlsx"
        attachment = open("/Users/stephanie/PycharmProjects/TwitterBot/venv/reports/weekly_trending_tweets2019-04-12 18:04:35.255748.xlsx", "rb")
        # instance of MIMEBase and named as p
        p = MIMEBase('application', 'octet-stream')

        # To change the payload into encoded form
        p.set_payload((attachment).read())

        # encode into base64
        encoders.encode_base64(p)

        p.add_header('Content-Disposition', "attachment; filename= %s" % filename)
        message_part = MIMEText(body, "html")
        message.attach(message_part)
        message.attach(p)
        with smtplib.SMTP_SSL("smtp.gmail.com", port, context=context) as server:
            server.login('cxmobileapps@gmail.com', password)
            server.sendmail(sender_email, receiver_email, message.as_string())
    except Exception as e:
        logging.exception('Error in sending weekly status update mail' + str(e))



#sendEmail(body)
sendWeeklyStatusTemp(None)
