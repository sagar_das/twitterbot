import tweepy
import time
import json
import smtplib
from SentiAn import get_tweet_sentiment
from operations import dboperations as dbo
from operations import marketanalysis as ma
from operations import replysentimentanalysis as rs
from operations import tweetanalysis as ta
from operations import gnewsscraper as gns
from operations import exceloperations as es
from operations import scraper
from messages import Messages
import logging
import threading
from datetime import datetime, date,time
from tornado import gen
import schedule
import pprint
import csv
import os
import pandas as pd
import platform
import tempfile
import sqlite3
from bs4 import BeautifulSoup
import re
import nltk




# Rob  "recipient_id": '517666370'

#Personal ID

# CONSUMER_KEY='2DLmm4btCkzIEuKZOEs5xSqdJ'
# CONSUMER_SECRET='3G9LSj5aMjKtuquu4Sr8IRk3EUCg7MA1ClOVleH8q9A4soYfKZ'
# ACCESS_KEY='2378707304-vRFzEMuqjZqKcZVhIr6BMW1y04u1W6wepqb7Wpb'
# ACCESS_SECRET='9LCYM9YDhGLibFDrrECrgchUPAM2hU5maJ8GkKmtf6tRc'

#CX User ID

CONSUMER_KEY='f7MbMQZNGNkP8ofhB4VOJospj'
CONSUMER_SECRET='tOrKfp5mnJnqGLcKjyVSSflzmzQNmBYwf0YF3JRSwmqdDuemHC'
ACCESS_KEY='1075443857238646784-H0ScXangYpsQcC3tr2ThkByv8P9IGl'
ACCESS_SECRET='K4099Yl2hIJtC9w2vfrPK55rulAWqADpmoof4uBG8Ua2m'

#Tom key
TOM_CONSUMER_KEY='Q8Bbs848wy5LZNsEg341IZDbN'
TOM_CONSUMER_SECRET='tDXuJLtJTMMAUbHC8HalBfrW7yT3oDtlho2Dw052Cvg1XAq3cV'
TOM_ACCESS_KEY='1115120603428556804-ddk0SDk66wowUPEMIzUak30skBideX'
TOM_ACCESS_SECRET='ST5xP5m2lt5ektWG3gQolzw4lqcEV42TwCNIQdSfX9Jsr'

file_name = 'last_seen_id.txt'
last_follower_file = 'last_follower_id.txt'

mAuth = tweepy.OAuthHandler(CONSUMER_KEY,CONSUMER_SECRET)
mAuth.set_access_token(ACCESS_KEY,ACCESS_SECRET)
mAPI= tweepy.API(mAuth,wait_on_rate_limit=True)

#Setting up API for replies
mReplies_Auth = tweepy.OAuthHandler(TOM_CONSUMER_KEY,TOM_CONSUMER_SECRET)
mReplies_Auth.set_access_token(TOM_ACCESS_KEY,TOM_ACCESS_SECRET)
mReplyAPI = tweepy.API(mReplies_Auth,wait_on_rate_limit=True)

#Initializing log operations
logging.basicConfig(filename='logs/cxmarketbot_'+str(datetime.now())+'.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s')
logging.error('Initializing log file')

# regionInitializing sqlite database
db = dbo.dboperations()
db.createTable()
dbLock = threading.RLock()

#endregion


event = {
  "event": {
    "type": "message_create",
    "message_create": {
      "target": {
        "recipient_id": '324537324'
      },
      "message_data": {
        "text": 'Hi, person xyz has expressed interest in cpq.'
      }
    }
  }
}




def formulateMessage(recipient_id,message_data_test):
    """

    :param recipient_id:
    :param message_data_test:
    :return:
    """
    event = {
        "event": {
            "type": "message_create",
            "message_create": {
                "target": {
                    "recipient_id": recipient_id
                },
                "message_data": {
                    "text": message_data_test
                }
            }
        }
    }
    return event



#region File operations

def saveLastSeenId(last_seen_id):
    global file_name

    with open(file_name,'w') as file:
        file.write(str(last_seen_id))

def saveLastFollowerID(last_follower_id):
    global last_follower_file

    with open(last_follower_file,'w') as file:
        file.write(str(last_follower_id))



def retrieveLastFollowerId():
    global last_follower_file

    last_follower_id = -1

    # file_read = open(file_name)
    # last_seen_id = int(file_read.read().strip())
    # file_read.close()

    with open(last_follower_file,'r') as file:
        data = file.read()
        if (data != ''):
            last_follower_id = int(data.strip())

    return last_follower_id


def retrieveLastSeenId():
    global file_name

    last_seen_id = -1

    # file_read = open(file_name)
    # last_seen_id = int(file_read.read().strip())
    # file_read.close()

    with open(file_name,'r') as file:
        data = file.read()
        if (data != ''):
            last_seen_id = int(data.strip())

    return last_seen_id

#endregion

last_seen_id = retrieveLastSeenId()



# mentions_dict = mentions[0].__dict__
#
# print(mentions_dict.keys())
# print(mentions_dict['text'])


# for mention in mentions:
#     print(str(mention.id)+'-'+mention.text)
#     if '#cpq' in mention.text.lower():
#         print('found')




def replyToTweets():
    global last_seen_id
    global event
    if last_seen_id == -1:
        mentions = mAPI.mentions_timeline(tweet_mode='extended')
    else:
        mentions = mAPI.mentions_timeline(last_seen_id, tweet_mode='extended')
    print('finding tweets')
    for mention in reversed(mentions):
        #print(str(mention.id)+ '-'+mention.full_text)
        print(str(mention.id) + '-' + mention.full_text)
        last_seen_id = mention.id
        saveLastSeenId(last_seen_id)
        if '#cpq' in mention.full_text.lower():
            print('tweet found. replying')
            mAPI.update_status('@' + mention.user.screen_name +' Hi. Here is our top 10 offerings in #cpq',mention.id)
            #mAPI.send_direct_message('@user_cx','hi')
            #mAPI.send_direct_message_new(event)
            message = 'Hi, @'+mention.user.screen_name+' has expressed interest in CPQ.'
#            sendDirectMessage(message)


def sendDirectMessage(message):
    stakeholder_user_id_list = Messages.stake_holders_user_ids
    for user_id in stakeholder_user_id_list:
        event = formulateMessage(user_id,message)
        mAPI.send_direct_message_new(event)

def getFollowers():
    return mAPI.followers()

def getEligibleTweetsFromDB():
    selectionArgs = {'SELECTED': 1}
    cursor = db.selectFromTable('TWEETS', selectionArgs=selectionArgs)


def sendMessageToNewFollower():
    try:
        selectionArgs = {'WELCOME_MSG_SENT':0}
        cursor = db.selectFromTable('FOLLOWERS',selectionArgs=selectionArgs)

        for follower in cursor:
            welcome_message = 'Hey @'+follower[2]+' '+Messages.new_follower_welcome_message
            event = formulateMessage(follower[0],welcome_message)
            mAPI.send_direct_message_new(event)

            #updating value in table after sending message
            setArgs = {'WELCOME_MSG_SENT':1}
            updateArgs = {'ID':follower[0]}
            result = db.updateTable('FOLLOWERS',setArgs,updateArgs=updateArgs)


    except Exception as e: logging.error(str(e))

#region Binary Search operations
def binarySearchFollowers(current_followers, l, r, active_follower_dict):
    # Check base case
    if r >= l:

        mid = l + (r - l) / 2

        # If element is present at the middle itself
        current_follower = current_followers[mid]

        if current_follower['ID'] == active_follower_dict['ID']:
            return mid

            # If element is smaller than mid, then it
        # can only be present in left subarray
        elif current_follower['ID']  > active_follower_dict['ID']:
            return binarySearchFollowers(current_followers, l, mid - 1, active_follower_dict)

            # Else the element can only be present
        # in right subarray
        else:
            return binarySearchFollowers(current_followers, mid + 1, r, active_follower_dict)

    else:
        # Element is not present in the array
        return -1

def binarySearchActiveFollowers(active_followers, l, r, follower_id):
    # Check base case
    if r >= l:

        mid = l + (r - l) / 2

        # If element is present at the middle itself

        if active_followers[mid] == follower_id:
            return mid

            # If element is smaller than mid, then it
        # can only be present in left subarray
        elif active_followers[mid] > follower_id:
            return binarySearchActiveFollowers(active_followers, l, mid - 1, follower_id)

            # Else the element can only be present
        # in right subarray
        else:
            return binarySearchActiveFollowers(active_followers, mid + 1, r, follower_id)

    else:
        # Element is not present in the array
        return -1
#endregion


#region Followers catch up
def maintainFollowers():
    try:
        cursor = db.selectFromTable('FOLLOWERS')
        #Current followers list will be fetched from DB sorted in ascending order as per the ID column
        current_followers_list = []
        if cursor == None:
            return
        for row in cursor:
            current_followers_list.append(row)

        followers_count = cursor.rowcount
        active_followers = getFollowers()
        active_followers_count = len(active_followers)
        active_followers_id_list =[]

        if followers_count == active_followers_count:
            return

        #If the table is empty
        if followers_count == -1 or followers_count ==0:
            for active_follower in active_followers:
                active_follower_dict = active_follower.__dict__
                result = db.insertIntoTable('FOLLOWERS',active_follower_dict)
            return


        #Checking the list of new followers
        if active_followers_count > followers_count:
            for active_follower in active_followers:
                active_follower_dict = active_follower.__dict__
                search_result = binarySearchFollowers(current_followers_list,0,len(current_followers_list) - 1,active_follower_dict)

                if search_result == -1:
                     result = db.insertIntoTable('FOLLOWERS',active_follower_dict)

        #Followers have unfollowed
        if active_followers_count < followers_count:
            for active_follower in active_followers:
                active_follower_dict = active_follower.__dict__
                active_followers_id_list.append(active_follower_dict['ID'])

             #sorting active followers id list
            active_followers_id_list.sort()

        #CHecking if followers have unfollowed
            for current_follower in current_followers_list:
                search_result = binarySearchActiveFollowers(active_followers_id_list,0,len(active_followers_id_list)-1,current_follower['ID'])

                if search_result == -1:
                    deleteArgs = {'ID':current_follower['ID']}
                    result = db.deleteFromTable('FOLLOWERS',deleteArgs=deleteArgs)

    except Exception as e : logging.exception(e)
#endregion


def retweetById(tweet_id):
    mAPI.retweet(tweet_id)

def getTweetsByHashTags(dbLock):
    while True:

        count = 0
        for queryString in Messages.hash_tags:
            try:
                print('Fetching tweets for #'+queryString)
                count+=1
                selectionArgs = {'HASH_TAG': queryString}
                dbLock.acquire()
                cursor = db.selectFromTable('TWEETS', selectionArgs=selectionArgs)
                dbLock.release()
                tweetId = None
                for row in cursor:
                    tweetId = row[0]
                    break

                fetchedTweets = mAPI.search(q = queryString, count = 200,since_id =tweetId)
                #mAPI.retweet
                tweets = []
                for tweet in fetchedTweets:
                    tweet_dict = tweet.__dict__
                    user = tweet_dict['user']
                    user_dict = user.__dict__
                    entities = tweet_dict['entities']
                    urls = entities['urls']
                    expanded_url =''
                    if len(urls) > 0:
                        expanded_url = urls[0]['expanded_url']

                    if user_dict['screen_name'] in Messages.blocked_by_users:
                        continue

                    #print ('user_screen_name: ' +str(user_dict['screen_name']))
                    tweet_status = mAPI.get_status(tweet_dict['id'],tweet_mode='extended')
                    tweet_status_dict = tweet_status.__dict__
                    tweet_record_vals ={'id':tweet_dict['id'],'text': tweet_status_dict['full_text'].replace("'","&quot"), 'hash_tag': queryString.lstrip('#')
                                        ,'favourite_count':tweet_dict['favorite_count'],
                                        'retweet_count':tweet_dict['retweet_count'],'expanded_url':expanded_url
                                        ,'user_id':user_dict['id'],
                                        'user_name':user_dict['name'].replace("'","&quot"),'user_screen_name':user_dict['screen_name'].replace("'","&quot"),
                                        'user_location':user_dict['location'].replace("'","&quot"),'user_description':user_dict['description'].replace("'","&quot"),'user_followers_count':user_dict['followers_count']}
                    #tweet_status = mAPI.get_status(tweet_dict['id'])
                    dbLock.acquire()
                    db.insertIntoTable('TWEETS',tweet_record_vals)
                    dbLock.release()

                    #print (tweet_dict['text'])
                    #Making the thread after every 5 hash tags
                if count%5 ==0:
                    print('Thread 1 is sleeping for 15 mins')
                    #t2.start()
                    time.sleep(900)
            except Exception as e : print(e)


def getTweetsByHashTag(queryString):
    try:
        fetchedTweets = mAPI.search(q = queryString, count = 200)
        #mAPI.retweet
        tweets = []
        for tweet in fetchedTweets:
            tweet_dict = tweet.__dict__
            user = tweet_dict['user']
            user_dict = user.__dict__
            entities = tweet_dict['entities']
            urls = entities['urls']
            expanded_url =''
            if len(urls) > 0:
                expanded_url = urls[0]['expanded_url']

            if user_dict['screen_name'] in Messages.blocked_by_users:
                continue

            print ('user_screen_name: ' +str(user_dict['screen_name']))
            tweet_status = mAPI.get_status(tweet_dict['id'],tweet_mode='extended')
            tweet_status_dict = tweet_status.__dict__
            tweet_record_vals ={'id':tweet_dict['id'],'text': tweet_status_dict['full_text'].replace("'","&quot"), 'hash_tag': queryString.lstrip('#')
                                ,'favourite_count':tweet_dict['favorite_count'],
                                'retweet_count':tweet_dict['retweet_count'],'expanded_url':expanded_url
                                ,'user_id':user_dict['id'],
                                'user_name':user_dict['name'].replace("'","&quot"),'user_screen_name':user_dict['screen_name'].replace("'","&quot"),
                                'user_location':user_dict['location'].replace("'","&quot"),'user_description':user_dict['description'].replace("'","&quot"),'user_followers_count':user_dict['followers_count']}
            #tweet_status = mAPI.get_status(tweet_dict['id'])
            db.insertIntoTable('TWEETS',tweet_record_vals)

            print (tweet_dict['text'])
    except Exception as e : print(e)

def getTweetsByQuery(queryString):
    fetchedTweets = mAPI.search(q = queryString, count = 200)
    tweets = []
    for tweet in fetchedTweets:
        parsedTweet ={}
        parsedTweet['text'] = tweet.text
        parsedTweet['sentiment'] = get_tweet_sentiment(tweet.text)
        tweets.append(parsedTweet)
        print('-----TWEET TEXT--------')
        print(tweet.text)
        print('-----SENTIMENT--------')
        print(parsedTweet['sentiment'])

def getTweetsFromUser(username):
    pp = pprint.PrettyPrinter(compact=True)
    try:

        select_query = "select ID from USER_TIMELINE where USER_SCREEN_NAME = '"+username+"' order by INSRTD_TMP_STMP desc limit 1"
        cursor = db.selectRawQuery(select_query)

        tweepy_cursor = None
        result_cursor = cursor.fetchall()
        if len(result_cursor) > 0:
            for row in result_cursor:
                tweet_id = str(row[0])
                tweepy_cursor = tweepy.Cursor(mAPI.user_timeline, screen_name=username,tweet_mode="extended",since_id=tweet_id).items()
        else:
            tweepy_cursor = tweepy.Cursor(mAPI.user_timeline, screen_name=username,tweet_mode="extended").items()



        for status in tweepy_cursor:
            pp.pprint(status._json)
            status_dict = status.__dict__

            entities = status_dict['entities']
            urls = entities['urls']

            expanded_url = ''
            media_url = ''
            if len(urls) > 0:
                expanded_url = urls[0]['expanded_url']
            try:
                media = entities['media']
                if len(media) > 0:
                    media_url = media[0]['media_url_https']
            except Exception as e : print('No media found')

            tweet_record_vals = {'id':status_dict['id'], 'text': status_dict['full_text'].replace("'","&quot"),
                                 'favourite_count': status_dict['favorite_count'],
                                 'retweet_count': status_dict['retweet_count'], 'expanded_url': expanded_url,
                                 'user_screen_name': username,
                                 'media_url': media_url
                                 }

            db.insertIntoTable('USER_TIMELINE', tweet_record_vals)

    except Exception as e : print(e)


        #print(status_dict.keys())

def getRepliesOfTweetById(tweet_id,user_id):
    replies = []
    #non_bmp_map = dict.fromkeys(range(0x10000, sys.maxunicode + 1), 0xfffd)

    for tweet in tweepy.Cursor(mReplyAPI.search, q='to:' + user_id, since_id=tweet_id,result_type='recent', timeout=999999).items(500):
        if hasattr(tweet, 'in_reply_to_status_id_str'):
            tweet_dict = tweet.__dict__
            user = tweet_dict['user']
            user_dict = user.__dict__
            entities = tweet_dict['entities']
            urls = entities['urls']
            expanded_url = ''
            if len(urls) > 0:
                expanded_url = urls[0]['expanded_url']
            if (tweet_dict['in_reply_to_status_id_str'] == str(tweet_id)):
                replies.append(tweet.text)
                tweet_status = mReplyAPI.get_status(tweet_dict['id'], tweet_mode='extended')
                tweet_status_dict = tweet_status.__dict__
                reply_record_vals = {'id': tweet_dict['id'],
                                     'parent_tweet_id' : tweet_id,
                                     'text': tweet_status_dict['full_text'].replace("'", "&quot")
                                     ,'favourite_count': tweet_dict['favorite_count'],
                                     'retweet_count': tweet_dict['retweet_count'], 'expanded_url': expanded_url
                    , 'user_id': user_dict['id'],
                                     'user_name': user_dict['name'].replace("'", "&quot"),
                                     'user_screen_name': user_dict['screen_name'].replace("'", "&quot"),
                                     'user_location': user_dict['location'].replace("'", "&quot"),
                                     'user_description': user_dict['description'].replace("'", "&quot"),
                                     'user_followers_count': user_dict['followers_count']}
                # tweet_status = mAPI.get_status(tweet_dict['id'])
                db.insertIntoTable('REPLIES', reply_record_vals)
                print('Replies inserted')
    #print("Tweet :", full_tweets.text.translate(non_bmp_map))
                for elements in replies:
                    print("Replies :", elements)
        replies.clear()

def getRepliesOfTweet(dbLock):
    replies = []
    #non_bmp_map = dict.fromkeys(range(0x10000, sys.maxunicode + 1), 0xfffd)

    #selectionArgs = {'HASH_TAG': 'salesforce'}
    #cursor = db.selectFromTable('TWEETS', selectionArgs=selectionArgs)
    while True:
        try:
            select_query = 'SELECT * FROM TWEETS WHERE hash_tag IS NOT NULL AND reply_fetch = 0'
            dbLock.acquire()
            cursor = db.selectRawQuery(select_query)
            dbLock.release()
            count = 0
            for ftweet in cursor:
                count+=1
                if count%150 == 0:
                    print('thread 2 sleeping for 15 mins')
                    time.sleep(900)

                if 'SaraCIMdata' in str(ftweet[6]):
                    continue
                for tweet in tweepy.Cursor(mReplyAPI.search, q='to:' + ftweet[6], since_id=ftweet[0],result_type='recent', timeout=999999).items(1000):
                    print('Fetch replies')
                    if hasattr(tweet, 'in_reply_to_status_id_str'):
                        tweet_dict = tweet.__dict__
                        user = tweet_dict['user']
                        user_dict = user.__dict__
                        entities = tweet_dict['entities']
                        urls = entities['urls']
                        expanded_url = ''
                        if len(urls) > 0:
                            expanded_url = urls[0]['expanded_url']
                        if (tweet_dict['in_reply_to_status_id_str'] == str(ftweet[0])):
                            replies.append(tweet.text)
                            tweet_status = mReplyAPI.get_status(tweet_dict['id'], tweet_mode='extended')
                            tweet_status_dict = tweet_status.__dict__
                            reply_record_vals = {'id': tweet_dict['id'],
                                                 'parent_tweet_id' : ftweet[0],
                                                 'text': tweet_status_dict['full_text'].replace("'", "&quot")
                                                 ,'favourite_count': tweet_dict['favorite_count'],
                                                 'retweet_count': tweet_dict['retweet_count'], 'expanded_url': expanded_url
                                , 'user_id': user_dict['id'],
                                                 'user_name': user_dict['name'].replace("'", "&quot"),
                                                 'user_screen_name': user_dict['screen_name'].replace("'", "&quot"),
                                                 'user_location': user_dict['location'].replace("'", "&quot"),
                                                 'user_description': user_dict['description'].replace("'", "&quot"),
                                                 'user_followers_count': user_dict['followers_count']}
                            # tweet_status = mAPI.get_status(tweet_dict['id'])
                            dbLock.acquire()
                            db.insertIntoTable('REPLIES', reply_record_vals)
                            dbLock.release()
                            #print("Tweet :", full_tweets.text.translate(non_bmp_map))

                            for elements in replies:
                                print("Replies :", elements)
                # updating value in tweets table after inserting tweets in replies table
                setArgs = {'REPLY_FETCH': 1}
                updateArgs = {'ID': ftweet[0]}
                dbLock.acquire()
                result = db.updateTable('TWEETS', setArgs, updateArgs=updateArgs)
                dbLock.release()
                replies.clear()
        except tweepy.TweepError as e:
            print(e.reason)
            print(e.response)
            print(e.response.content)
        except Exception as e:
            print(e)


def followUser(userid):
    mAPI.create_friendship(userid)


def createDailyReport():

    trending_tweets_list = []
    potential_users_list = []
    excluded_list =['cpq','crm','qtc']
    for hash_tag in Messages.hash_tags:

        if any(eHashTag in hash_tag for eHashTag in excluded_list):
            continue

        mObject = ma.marketanalysis()
        eligible_list = mObject.getTrendingTweetsByHashTag(hash_tag)

        print('Processing for '+hash_tag)

        for i in range(0,3):
            if i == len(eligible_list):
                break;
            print('processing '+str(i+1))
            user_list = mObject.getUserDetails(eligible_list[i]['tweet_text'],hash_tag)
            potential_users_list+=user_list
            print('user list fetched')
            retweeted_tweet = mAPI.get_status(eligible_list[i]['retweet_tweet_id'])
            selectionArgs = {'ID': retweeted_tweet.retweeted_status.id}
            original_tweet = mAPI.get_status(retweeted_tweet.retweeted_status.id, tweet_mode='extended')
            tweet_dict = original_tweet.__dict__
            user = tweet_dict['user']
            user_dict = user.__dict__
            entities = tweet_dict['entities']
            urls = entities['urls']
            expanded_url = ''
            if len(urls) > 0:
                expanded_url = urls[0]['expanded_url']

            if user_dict['screen_name'] in Messages.blocked_by_users:
                continue

            # print ('user_screen_name: ' +str(user_dict['screen_name']))

            #tweet_status_dict = tweet_status.__dict__
            tweet_record_vals = {'id': tweet_dict['id'], 'text': tweet_dict['full_text'].replace("'", "&quot"),
                                 'hash_tag': hash_tag
                , 'favourite_count': tweet_dict['favorite_count'],
                                 'retweet_count': tweet_dict['retweet_count'], 'expanded_url': expanded_url
                , 'user_id': user_dict['id'],
                                 'user_name': user_dict['name'].replace("'", "&quot"),
                                 'user_screen_name': user_dict['screen_name'].replace("'", "&quot"),
                                 'user_location': user_dict['location'].replace("'", "&quot"),
                                 'user_description': user_dict['description'].replace("'", "&quot"),
                                 'user_followers_count': user_dict['followers_count']}
            result = db.insertIntoTable('TWEETS', tweet_record_vals)
            if result == 'success':
                print('Inserted to DB')

            print('original tweet details fetched')

            print('fetching replies')
            getRepliesOfTweetById(original_tweet.id,original_tweet.user.screen_name)
            print('replies fetch complete')

            select_query = "SELECT COUNT(*) FROM REPLIES WHERE PARENT_TWEET_ID ='"+str(original_tweet.id)+"'"
            cursor = db.selectRawQuery(select_query)
            reply_count = 0
            postive_reply_percent = 0
            negative_reply_percent = 0
            for row in cursor:
                reply_count = row[0]

            '''If count of replies is greater than 0, fetch all the replies'''
            if reply_count > 0:
                print('performing sentiment analysis')
                reply_sentiment_dict = classifySentiment(original_tweet.id)
                postive_reply_percent = (reply_sentiment_dict['positive_count']/reply_sentiment_dict['total_count'])*100
                negative_reply_percent = (reply_sentiment_dict['negative_count'] / reply_sentiment_dict['total_count']) * 100

            trending_tweet_dict ={
                'topic': hash_tag,
                'tweet_url': "https://twitter.com/i/web/status/"+str(original_tweet.id),
                'tweet_text': original_tweet.full_text,
                'tweeted_by': user_dict['screen_name'],
                'retweet_count': original_tweet.retweet_count,
                'favourites_count': original_tweet.favorite_count,
                'no_of_replies': reply_count,
                'postive_reply_percent': postive_reply_percent,
                'negative_reply_percent': negative_reply_percent
            }
            trending_tweets_list.append(trending_tweet_dict)
        print('completed for '+str(i+1))

    print("Dumping to json files")

    with open('/Users/stephanie/PycharmProjects/TwitterBot/venv/reports/tweets_'+str(datetime.now())+'.json','w') as tweet_json_file:
        json.dump(trending_tweets_list,tweet_json_file)

    with open('/Users/stephanie/PycharmProjects/TwitterBot/venv/reports/users_'+str(datetime.now())+'.json','w') as users_json_file:
        json.dump(potential_users_list,users_json_file)

def createWeeklyReport():
    excluded_list = ['comcast']
    weekly_topic_list =[]
    mObject = ma.marketanalysis()
    tObject = ta.tweetanalysis()
    for hash_tag in Messages.hash_tags:
        if any(eHashTag in hash_tag for eHashTag in excluded_list):
            continue
        tweet_text_list = mObject.getWeeklyTweetsByHashTag(hash_tag)
        final_words_list = []

        for tweet_text in  tweet_text_list:
            final_words_list += tObject.frequencyDistOfText(str(tweet_text).lower(),hash_tag)

        frequent_hashtags, frequent_users, frequent_words = tObject.findRelatedTopics(final_words_list)

        weekly_topic_dict = {
            'topic' : hash_tag,
            'frequent_hashtags': frequent_hashtags,
            'frequent_users': frequent_users,
            'frequent_words': frequent_words
        }

        weekly_topic_list.append(weekly_topic_dict)

    print("Dumping to json files")

    with open('/Users/stephanie/PycharmProjects/TwitterBot/venv/reports/topics_weekly_' + str(datetime.now()) + '.json',
              'w') as tweet_json_file:
        json.dump(weekly_topic_list, tweet_json_file)

def scrapeGNews():
    try:
        gObject = gns.gnewsscraper()
        query = 'comcast'

        all_news_articles = gObject.getDetailedArticles(query)

        articles = all_news_articles['articles']

        id = 1

        selectQuery = 'SELECT ID FROM gnews ORDER BY INSRTD_TMP_STMP DESC LIMIT 1'

        cursor = db.selectRawQuery(selectQuery)

        try:
            for row in cursor:
                id = row[0]
        except Exception as e :print('No existing records found')

        for article in articles:

            source = article['source']
            article_vals = {
                'id': str(id),
                'author': str(article['author']).replace("'","&quot"),
                'title': str(article['title']).replace("'","&quot"),
                'content': str(article['content']).replace("'","&quot"),
                'description': str(article['description']).replace("'","&quot"),
                'published_at': str(article['publishedAt']),
                'source': str(source['name']).replace("'","&quot"),
                'url': article['url'],
                'url_to_image': '' if article['urlToImage'] is None else article['urlToImage']


            }

            result = db.insertIntoTable('GNEWS', article_vals)
            id += 1


    except Exception as e: print(e)


def classifySentiment(parent_tweet_id):
    selectionArgs = {'PARENT_TWEET_ID': parent_tweet_id}
    cursor = db.selectFromTable('REPLIES', selectionArgs=selectionArgs)
    rObject = rs.replysentimentanalysis()
    total_count = 0
    positive_count = 0
    negative_count = 0
    for row in cursor:
        total_count+=1
        pred_dict = rObject.classifySentiment(row[2])
        if pred_dict['sentiment'] == 'positive':
            positive_count+=1
        elif pred_dict['sentiment'] == 'negative':
            negative_count+=1
        print(pred_dict)

    print('Results-----')
    print('Total positive percentage '+str((positive_count/total_count)*100))
    print('Total negative percentage '+str((negative_count/total_count)*100))

    result_dict = {
        'total_count': total_count,
        'positive_count':positive_count,
        'negative_count':negative_count
    }

    return result_dict

def classifySentimentTimeline():
    rObject = rs.replysentimentanalysis()
    df = pd.read_csv('/Users/stephanie/PycharmProjects/TwitterBot/venv/data/timeline_data/comcast_timeline_tweets.csv',
                     encoding="latin-1")

    field_names = ['tweet_id', 'tweet_text', 'result']

    with open('/Users/stephanie/PycharmProjects/TwitterBot/venv/data/timeline_data/comcast_timeline_tweets_results'
              + '.csv', 'w') as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=field_names)

        writer.writeheader()
        for i, row in df.iterrows():
            comment = row['tweet_text']
            pred_dict = rObject.classifySentiment(comment)
            writer.writerow({field_names[0]: row['tweet_id'], field_names[1]: row['tweet_text'], field_names[2]: pred_dict['sentiment']})


def getStrategyTweets():

    selected_tweet_index = []
    tweets_val =[]
    final_tweets_val =[]

    #reading results
    with open('/Users/stephanie/PycharmProjects/TwitterBot/venv/models/bert_model_phase_3/test_results.tsv') as tsvfile:
        reader = csv.reader(tsvfile, delimiter='\t')
        row_index = -1
        for row in reader:
            row_index+=1
            if float(row[0]) > 0.80:
                selected_tweet_index.append(row_index)


    print(len(selected_tweet_index))

    #reading test sheet tweets
    with open('/Users/stephanie/PycharmProjects/TwitterBot/venv/bert_data/test.tsv') as tsvfile:
        reader = csv.reader(tsvfile, delimiter='\t')
        row_index = -1
        for row in reader:
            row_index+=1
            if len(row)==0 or row_index==0:
                continue

            tweet ={
                'tweet_id': row[0],
                'tweet_url': 'https://twitter.com/i/web/status/'+row[0],
                'tweet_text': row[1]
            }
            tweets_val.append(tweet)

        for tweet_index in selected_tweet_index:
            final_tweets_val.append(tweets_val[tweet_index])

    print(len(tweets_val))
    print(len(final_tweets_val))

    with open('/Users/stephanie/PycharmProjects/TwitterBot/venv/reports/strategy_daily_' + str(datetime.now()).replace(':','.') + '.json',
              'w') as tweet_json_file:
        json.dump(final_tweets_val, tweet_json_file)

def scrapeDataSpider():
    os.system("scrapy runspider /Users/stephanie/PycharmProjects/TwitterBot/venv/operations/scraper.py")

def scrapeData():
    sObj = scraper.Scraper("https://www.g2.com/products/apttus-cpq/reviews?focus_review=285051&page=","&product_id=apttus-cpq")
    sObj.scrapeUsers()

def findKeyPeople(query,company_name):
    users = mAPI.search_users(query)
    for user in users:
        user_dict = user.__dict__
        print(user_dict['description'])

def AmazonJsonToCSV():
    csv_headers = ['rating','text']
    with open('/Users/stephanie/PycharmProjects/TwitterBot/venv/data/amazon_data/echo_3_data.json','r') as json_file:
        raw_data = json.load(json_file)

        with open('/Users/stephanie/PycharmProjects/TwitterBot/venv/data/amazon_data/amazon_reviews.csv','w') as csv_file:
            writer = csv.DictWriter(csv_file, fieldnames=csv_headers)

            writer.writeheader()

            for page in raw_data:
                reviews = page['reviews']

                for review in reviews:
                    review_text = str(review['review_text']).replace(",","")
                    writer.writerow({csv_headers[0]:review['review_rating'],csv_headers[1]:review_text})

def AmazonEvalSentiment():

    sentiment_classes = []
    # reading results
    with open('/Users/stephanie/PycharmProjects/TwitterBot/venv/models/amazon_review_bert_model/test_results.tsv') as tsvfile:
        reader = csv.reader(tsvfile, delimiter='\t')

        for row in reader:
            sc = 0 if row[0] > row[1] else 1
            sentiment_classes.append(sc)

    for sc in sentiment_classes:
        print(sc)

def createTempFile(name,extension,content):
    temp = tempfile.NamedTemporaryFile(prefix=name, suffix=extension,mode="w+t")
    blobfile = None

    try:
        temp.writelines(content)
        temp.seek(0)
        blobfile = temp.read()
    finally:
        temp.close()

    return blobfile

def saveNewsArticle():
    gObj = gns.gnewsscraper()
    select_query = 'SELECT * FROM GNEWS WHERE SCRAPED_DATA IS NULL LIMIT 100'
    cursor = db.selectRawQuery(select_query)

    try:
        for news in cursor:
            url = str(news[7])
            response = gObj.scrapeArticleData(url)
            #blob_file = createTempFile("GNEWS_ID_"+str(news[0]),".html",str(response))

            blob_file = str.encode(response)
            # updating value in table after sending message
            setArgs = {'SCRAPED_DATA': sqlite3.Binary(blob_file)}
            updateArgs = {'ID': news[0]}
            result = db.updateTableBlob('GNEWS', setArgs, updateArgs=updateArgs)
            print(result)
    except Exception as e:
        print(e)

def processNewsArticle():

    # TODO: Add date range
    query = "SELECT * FROM GNEWS WHERE SCRAPED_DATA IS NOT NULL"
    cursor = db.selectRawQuery(query)

    extracted_links = []

    for row in cursor:
        val = (row[10]).decode("utf-8")
        #Extracting all links in the page
        soup = BeautifulSoup(val,features="lxml")
        for link in soup.findAll('link', attrs={'href': re.compile("^https://")}):
            extracted_links.append(link.get('href'))

        #Finding the occurence of comment section
        soup.body.findAll(text=re.compile('^Comment$'))

def SentimentVitals():
    '''
    0 - Negative Sentiment
    1 - Positive Sentiment
    :return:
    '''
    data_frame = pd.read_csv('/Users/stephanie/PycharmProjects/TwitterBot/venv/data/cpq_data/cpq_apttus_g2.csv',delimiter=',')
    tObj = ta.tweetanalysis()
    count_total = len(data_frame.index)
    print("Total Number of rows "+str(count_total))
    neg_data_frame = data_frame[data_frame['class']== 0]
    pos_data_frame = data_frame[data_frame['class'] == 1]
    count_neg = len(neg_data_frame.index)
    count_pos = len(pos_data_frame.index)
    print("Total Number of +ve rows " + str(count_pos))
    print("Total Number of -ve rows " + str(count_neg))
    print("Percentage of +ve rows " + str((count_pos/count_total)*100)+"%")
    print("Percentage of -ve rows " + str((count_neg/count_total)*100)+"%")

    stopwords = set(nltk.corpus.stopwords.words('english'))
    #Analyzing for Negative reviews
    print("---Negative reviews analysis--")
    final_words_list = []
    nouns_list = []
    proper_nouns_list = []
    adjectives_list = []
    integrate_set= set()
    for index,row in neg_data_frame.iterrows():
        text = str(row['text'])
        for noun in tObj.findNouns(str(text)):
            nouns_list.append(noun)

        for noun in tObj.findProperNouns(str(text)):
            proper_nouns_list.append(noun)

        for adjective in tObj.findAdjectives(str(text)):
            adjectives_list.append(adjective)

        if "integr" in text:
            integrate_set.add(text)

        words_list = text.split()
        text = ""
        for word in words_list:
            if word in stopwords:
                continue
            text+=word+" "

        final_words_list.append(text)

    print("Frequently used N-grams")
    tObj.findFrequentNgrams(final_words_list)



    with_stop,without_stop = tObj.frequentWords(' '.join(nouns_list))

    print('Frequent Nouns')
    print(without_stop)

    with_stop, without_stop = tObj.frequentWords(' '.join(proper_nouns_list))
    print('Frequent Proper Nouns')
    print(without_stop)

    with_stop, without_stop = tObj.frequentWords(' '.join(adjectives_list))
    print('Frequent adjectives')
    print(without_stop)

    print("Reviews mentioning the frequently used adjectives")
    adj_sent_set = set()
    for index,row in neg_data_frame.iterrows():
        text = str(row['text'])

        # for noun in tObj.findNouns(str(text)):
        #     nouns_list.append(noun)
        #
        # for noun in tObj.findProperNouns(str(text)):
        #     proper_nouns_list.append(noun)

        for sentence in nltk.sent_tokenize(text):
            for adjective in without_stop:
                if str(adjective[0]) in str(sentence):
                    adj_sent_set.add(str(sentence))

    print(*adj_sent_set,sep='\n')

    # print('Frequent Nouns in Freq adj sentences')
    # print(without_stop)
    #
    # with_stop, without_stop = tObj.frequentWords(' '.join(proper_nouns_list))
    # print('Frequent Proper Nouns in Freq adj sentences')
    # print(without_stop)


    print("**Reviews about product integrations**")
    print(*integrate_set,sep="\n")
    nouns_set = set()
    proper_nouns_set = set()
    for text in integrate_set:
        for noun in tObj.findNouns(str(text)):
            nouns_set.add(noun)

        for noun in tObj.findProperNouns(str(text)):
            proper_nouns_set.add(noun)

    print("Nouns in integration")
    print(nouns_set)
    print("Proper Nouns in integration")
    print(proper_nouns_set)


    # Analyzing for Positive reviews
    print("---Positive reviews analysis--")
    tObj = ta.tweetanalysis()
    final_words_list = []
    nouns_list = []
    proper_nouns_list = []
    adjectives_list = []
    integrate_set = set()
    for index, row in pos_data_frame.iterrows():
        text = str(row['text'])

        if "integr" in text:
            integrate_set.add(text)

        for noun in tObj.findNouns(str(text)):
            nouns_list.append(noun)
        # nouns_list.append(tObj.findNouns(str(text)))
        for noun in tObj.findProperNouns(str(text)):
            proper_nouns_list.append(noun)
        # proper_nouns_list.append(tObj.findProperNouns(str(text)))

        for adjective in tObj.findAdjectives(str(text)):
            adjectives_list.append(adjective)

        for word in words_list:
            if word in stopwords:
                continue
            text+=word+" "

        final_words_list.append(text)

    print("Frequently used N-grams")
    tObj.findFrequentNgrams(final_words_list)

    with_stop, without_stop = tObj.frequentWords(' '.join(nouns_list))

    print('Frequent Nouns')
    print(without_stop)

    with_stop, without_stop = tObj.frequentWords(' '.join(proper_nouns_list))
    print('Frequent Proper Nouns')
    print(without_stop)

    with_stop, without_stop = tObj.frequentWords(' '.join(adjectives_list))
    print('Frequent adjectives')
    print(without_stop)

    print("Reviews mentioning the frequently used adjectives")
    adj_sent_set = set()
    for index, row in pos_data_frame.iterrows():
        text = str(row['text'])

        for sentence in nltk.sent_tokenize(text):
            for adjective in without_stop:
                if str(adjective[0]) in str(sentence):
                    adj_sent_set.add(str(sentence))

    print(*adj_sent_set, sep='\n')

    print("**Reviews about product integrations**")
    print(*integrate_set, sep="\n")
    nouns_set = set()
    proper_nouns_set = set()
    for text in integrate_set:
        for noun in tObj.findNouns(str(text)):
            nouns_set.add(noun)

        for noun in tObj.findProperNouns(str(text)):
            proper_nouns_set.add(noun)

    print("Nouns in integration")
    print(nouns_set)
    print("Proper Nouns in integration")
    print(nouns_set)

def tempOperations2():
    words = ['cpq','qtc','quote','cash','configure','price']
    path = "/Users/stephanie/PycharmProjects/TwitterBot/venv/data/timeline_data/"
    for i in os.listdir(path):
        if '.DS_Store' in str(i):
            continue
        tweet_list = []
        data_frame = pd.read_csv(path+str(i),
                                 delimiter=',')

        for index,row in data_frame.iterrows():
            text = str(row["tweet_text"])

            for word in words:
                if word in text:
                    tweet = {"tweet_id":row["tweet_id"],
                             "tweet_text":row["tweet_text"],
                             "user_screen_name":row["user_screen_name"]}
                    tweet_list.append(tweet)
                    break

        if len(tweet_list) > 0:
            print(*tweet_list,sep='\n')

def tempOperations():
    csv_headers = ['review_id', 'text','class']
    with open('/Users/stephanie/PycharmProjects/TwitterBot/venv/data/cpq_data/cpq_salesforce_g2.json', 'r') as json_file:
        raw_data = json.load(json_file)

        with open('/Users/stephanie/PycharmProjects/TwitterBot/venv/data/cpq_data/cpq_salesforce_g2.csv',
                  'w') as csv_file:
            writer = csv.DictWriter(csv_file, fieldnames=csv_headers)

            writer.writeheader()

            count = 0
            for reviews in raw_data:
                for review_dict in reviews:
                    postive_review_list = review_dict['positive']
                    negative_review_list = review_dict['negative']

                    for review in postive_review_list:
                        count+=1
                        review_text = str(review).replace(",", "").replace('"','')
                        writer.writerow({csv_headers[0]: str(count), csv_headers[1]: review_text,csv_headers[2]: str(1)})

                    for review in negative_review_list:
                        count+=1
                        review_text = str(review).replace(",", "").replace('"','')
                        writer.writerow({csv_headers[0]: str(count), csv_headers[1]: review_text,csv_headers[2]: str(0)})




tObj = ta.tweetanalysis()

# str1 = "AI is our friend and it has been friendly"
# str2 = "AI and humans have always been friendly"

# str1 = "Salesforce is buying data visualization company Tableau for $15.7B in all-stock deal"
# str2 = "All You Need to Know About Salesforce’s $15.7 Billion Acquisition of Tableau"
#
# print("Jaccard similarity "+str(tObj.get_jaccard_similarity(str1,str2)))
# print("Cosine similarity "+str(tObj.get_cosine_similarity(str1,str2)))

scrapeData()

















#getTweetsFromUser('comcast')

#scrapeGNews()
# mObject = ma.marketanalysis()
# val = mObject.getAndSaveDailyTweetsByHashTag(['salesforce','retail'])

#classifySentimentTimeline()
















