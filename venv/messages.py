class Messages:
    new_follower_welcome_message = 'thank you for following CommerceCX.  To learn more about what we do and see some of the great work we have done, check out. https://commercecx.com/.  Feel free to contact us if there is anything we can do for you.'
    user_notify_message = 'has expressed interest in Commerce'
    cms_website = 'https://www.cmswire.com/'
    cx_website = 'https://commercecx.com/blog/'
    hash_tags =['salesforce','salesforcetour','salesforceworldtour','digitaltransformation','apttus','cpq','qtc','commerceecosystem','crm','cloud','customerexperience','platform','retail',
                'ecommerce','retailsales','store','magento','wealthmanagement']
    sf_hash_tags = ['salesforce','salesforcetour','salesforceworldtour']
    #Stakeholders user id [Rob,Vinay]
    stake_holders_user_ids = [517666370,324537324]
    blocked_by_users = ['Axia_CPQ']
    excluded_characters =[
            ",",
            ".",
            "``",
            "''",
            ";",
            "?",
            "--",
            ")",
            "(",
            ":",
            "!",
            "-",
            "&amp;"
        ]
    revenue_synonyms =['revenue','increase','decrease','increment','decrement','enlarge','expand','grow','swell','expand','swell', 'reduce', 'drop', 'diminish', 'decline', 'dwindle', 'contract', 'shrink']
    stock_synonyms = ['stock','invest']
    money_synonyms = ['$','dollar']
    merger_synonyms = ['merger','alliance','partner','amalgamation', 'combination','merging', 'union', 'fusion', 'coalition', 'affiliation', 'coupling', 'unification', 'incorporation', 'coalescence', 'consolidation', 'confederation', 'hook-up', 'link-up']
    blacklist_news_sources = ['fark.com']
