import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from pandas import DataFrame

le = LabelEncoder()
df = pd.read_csv('/Users/stephanie/PycharmProjects/TwitterBot/venv/models/amazon_alexa_reviews_train.csv')

#df['tweet_text'] = df['tweet_text'].str.replace(" ","",1)

# Creating train and dev dataframes according to BERT
# df_bert = pd.DataFrame({'user_id':df['User_ID'],
#             'label':le.fit_transform(df['Is_Response']),
#             'alpha':['a']*df.shape[0],
#             'text':df['Description'].replace(r'\n',' ',regex=True)})

df_bert = pd.DataFrame({'tweet_id':df['tweet_id'],
            'label':le.fit_transform(df['tweet_class']),
            'alpha':['a']*df.shape[0],
            'text':df['tweet_text'].replace(r'\n',' ',regex=True)})

df_bert_train , df_bert_dev = train_test_split(df_bert, test_size=0.2)

#Test data frame
df_test = pd.read_csv("/Users/stephanie/PycharmProjects/TwitterBot/venv/models/cpq_tweets.csv")
df_test['tweet_text'] = df_test['tweet_text'].str.replace(" ","",1)
df_bert_test = pd.DataFrame({'tweet_id':df_test['tweet_id'],
                 'text':df_test['tweet_text'].replace(r'\n',' ',regex=True)})

#df_bert_train[:125].to_csv('/Users/stephanie/PycharmProjects/TwitterBot/venv/bert_data/train.tsv', sep='\t', index=False, header=False)
df_bert_train.to_csv('/Users/stephanie/PycharmProjects/TwitterBot/venv/bert_data/train.tsv', sep='\t', index=False, header=False)
#df_bert_dev[:25].to_csv('/Users/stephanie/PycharmProjects/TwitterBot/venv/bert_data/dev.tsv',sep='\t', index=False,header=False)
df_bert_dev.to_csv('/Users/stephanie/PycharmProjects/TwitterBot/venv/bert_data/dev.tsv',sep='\t', index=False,header=False)
df_bert_test.to_csv('/Users/stephanie/PycharmProjects/TwitterBot/venv/bert_data/test.tsv',sep='\t',index=False,header=True)

