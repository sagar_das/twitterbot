import os

data_dir = '/Users/stephanie/PycharmProjects/TwitterBot/venv/bert_data/'
#pretrained_model = '/Users/stephanie/code_repo/dataset/bert_models/cased_L-12_H-768_A-12/'
downloaded_pretrained_model = '/Users/stephanie/code_repo/dataset/bert_models/cased_L-12_H-768_A-12/'
pretrained_model = '/Users/stephanie/PycharmProjects/TwitterBot/venv/models/bert_model_phase_3/'
vocab_file=downloaded_pretrained_model+'vocab.txt'
bert_config_file = downloaded_pretrained_model+'bert_config.json'
#init_checkpoint=pretrained_model+'bert_model.ckpt'
init_checkpoint=pretrained_model+'model.ckpt-44'
#output_dir='/Users/stephanie/PycharmProjects/TwitterBot/venv/models/bert_models/'
output_dir='/Users/stephanie/PycharmProjects/TwitterBot/venv/models/bert_model_phase_3/'

'''COLA - Corpus of Linguistic acceptability'''

os.system("python /Users/stephanie/code_repo/bert/run_classifier.py "
          "--task_name=cola "
          "--do_train=false "
          "--do_eval=false "
          "--do_predict=true "
          "--data_dir="+data_dir+
          " --vocab_file="+vocab_file+
          " --bert_config_file="+bert_config_file+
          " --init_checkpoint="+init_checkpoint+
          " --max_seq_length=400 "
          "--train_batch_size=8 "
          "--learning_rate=2e-5 "
          "--num_train_epochs=3.0 "
          "--output_dir="+output_dir+
          " --do_lower_case=False")