from chatterbot import ChatBot
from chatterbot.trainers import ListTrainer, ChatterBotCorpusTrainer
from conversations import conversations

# Create a new chat bot named Charlie
chatbot = ChatBot('Charlie',
                  storage_adapter='chatterbot.storage.SQLStorageAdapter',
                  database_uri='sqlite:///database.sqlite3',
                  logic_adapters=[{
                    'import_path':'chatterbot.logic.BestMatch',
                    'default_response':'I\'m afraid I don\'t understand'
                  }
                ],
                  preprocessors=['chatterbot.preprocessors.convert_to_ascii']
                  )

trainer = ListTrainer(chatbot)
corpus_trainer = ChatterBotCorpusTrainer(chatbot)

#corpus_trainer.train("chatterbot.corpus.english")

trainer.train(conversations.conv_1)
trainer.train(conversations.conv_2)

#Getting response
while True:
    try:
        user_input = input()
        bot_response = chatbot.get_response(user_input)

        print(bot_response)

    except(KeyboardInterrupt, EOFError, SystemExit):
        break

# Get a response to the input text 'I would like to book a flight.'
# response = chatbot.get_response('What are your offerings in cognitive technologies?')
#
# print(response)