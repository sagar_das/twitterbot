import gensim
import numpy as np
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score,silhouette_samples
from scipy.spatial.distance import cdist
import pandas as pd
import warnings
import joblib
from datetime import datetime,date,time
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from sklearn.mixture.gaussian_mixture import GaussianMixture
from matplotlib.patches import Ellipse
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
import csv
import tweetanalysis as ta
from textblob import TextBlob
import nltk
import json



# file = gensim.models.word2vec.Text8Corpus('/Users/stephanie/Code_Repo/Dataset/k_means_clustering/text8')
# model = gensim.models.Word2Vec(file,size=150)
# print('Generating Gensim Model')
# model.save('/Users/stephanie/PycharmProjects/TwitterBot/venv/models/gensim/text8_150_gensim')
#
# analyzer = SentimentIntensityAnalyzer()


def classifyBasedOnKeyword(sentence):
    revenue_synonyms = ['revenue', 'increase', 'decrease', 'increment', 'decrement', 'enlarge', 'expand', 'grow',
                        'swell', 'expand', 'swell', 'reduce', 'drop', 'diminish', 'decline', 'dwindle', 'contract',
                        'shrink']
    stock_synonyms = ['stock', 'invest']
    money_synonyms = ['$']
    merger_synonyms = ['merger', 'alliance', 'partner', 'amalgamation', 'combination', 'merging', 'union', 'fusion',
                       'coalition', 'affiliation', 'coupling', 'unification', 'incorporation', 'coalescence',
                       'consolidation', 'confederation', 'hook-up', 'link-up']

    word_list = sentence.split(" ")
    result = []
    f_res = False
    # #If it is a number, then probability is high that it's revenue data
    # for word in word_list:
    #     f_res = isFloat(word)
    #     if f_res:
    #         result.append('revenue')
    #         break


    #Searching for revenue related keywords
    if 'revenue' in sentence:
        result.append('revenue')



    #Searching for stock related keywords
    for stock_word in stock_synonyms:
        if stock_word in sentence:
            result.append('stock')
            break

    # Searching for merger related keywords
    for merge_word in merger_synonyms:
        if merge_word in sentence:
            result.append('merger')
            break



    return result


def isFloat(word):

    try:
        float(word)
        return True
    except Exception as e:
        return False



def sentiment_scores(sentence):
    # Create a SentimentIntensityAnalyzer object.
    sid_obj = SentimentIntensityAnalyzer()

    result = ''

    # polarity_scores method of SentimentIntensityAnalyzer
    # oject gives a sentiment dictionary.
    # which contains pos, neg, neu, and compound scores.
    sentiment_dict = sid_obj.polarity_scores(sentence)

    print("Overall sentiment dictionary is : ", sentiment_dict)
    print("sentence was rated as ", sentiment_dict['neg'] * 100, "% Negative")
    print("sentence was rated as ", sentiment_dict['neu'] * 100, "% Neutral")
    print("sentence was rated as ", sentiment_dict['pos'] * 100, "% Positive")

    print("Sentence Overall Rated As", end=" ")

    # decide sentiment as positive, negative and neutral
    if sentiment_dict['compound'] >= 0.05:
        print("Positive")
        result = "Positive"

    elif sentiment_dict['compound'] <= - 0.05:
        print("Negative")
        result = "Negative"

    else:
        print("Neutral")
        result= "Neutral"

    return sentiment_dict,result

def print_sentiment_scores(sentence):
    snt = analyzer.polarity_scores(sentence)
    print("{:-<40} {}".format(sentence, str(snt)))

def get_vader_sentiment(sentence):
    return analyzer.polarity_scores(sentence)


def draw_ellipse(position, covariance, ax=None, **kwargs):
    """Draw an ellipse with a given position and covariance"""
    ax = ax or plt.gca()

    # Convert covariance to principal axes
    if covariance.shape == (2, 2):
        U, s, Vt = np.linalg.svd(covariance)
        angle = np.degrees(np.arctan2(U[1, 0], U[0, 0]))
        width, height = 2 * np.sqrt(s)
    else:
        angle = 0
        width, height = 2 * np.sqrt(covariance)

    # Draw the Ellipse
    for nsig in range(1, 4):
        ax.add_patch(Ellipse(position, nsig * width, nsig * height,
                             angle, **kwargs))


def plot_gmm(gmm, X, label=True, ax=None):
    ax = ax or plt.gca()
    labels = gmm.fit(X).predict(X)
    if label:
        ax.scatter(X[:, 0], X[:, 1], c=labels, s=40, cmap='viridis', zorder=2)
    else:
        ax.scatter(X[:, 0], X[:, 1], s=40, zorder=2)
    ax.axis('equal')

    w_factor = 0.2 / gmm.weights_.max()
    for pos, covar, w in zip(gmm.means_, gmm.covars_, gmm.weights_):
        draw_ellipse(pos, covar, alpha=w * w_factor)



def clusterYoutubeComments():
    '''Hide run tie warnings'''
    warnings.filterwarnings("ignore")

    '''load gensim model'''
    fname = "/Users/stephanie/PycharmProjects/TwitterBot/venv/models/gensim/text8_gensim"
    model = gensim.models.Word2Vec.load(fname)
    print("Gensim model has been loaded")

    '''reading from test data'''
    df = pd.read_csv('/Users/stephanie/PycharmProjects/TwitterBot/venv/clustering_data/Youtube04-Eminem.csv', encoding="latin-1")

    print ('Data frame shape '+ str(df.shape))
    dfList = list(df['CONTENT'])
    print('list length ' + str(len(dfList)))
    length = 0
    for data in dfList:
        length+= len(data)

    length = length/len(dfList)

    print('length '+str(length))


    df = df.drop(['COMMENT_ID', 'AUTHOR', 'DATE'], axis=1)
    original_df = pd.DataFrame(df)
    df = df.drop(['CLASS'],axis=1)

    '''Preparing data in correct format for clustering'''
    final_data = []

    for i,row in df.iterrows():
        comment_vectorized =[]
        comment = row['CONTENT']
        comment_all_words = comment.split(sep=" ")

        for comment_w in comment_all_words :
            try:
                comment_vectorized.append(list(model[comment_w]))
            except Exception as e:
                print(e)

        try:
            comment_vectorized = np.asarray(comment_vectorized)
            comment_vectorized_mean = list(np.mean(comment_vectorized,axis=0))
        except Exception as e:
            comment_vectorized_mean = list(np.zeros(100))

        try:
            len(comment_vectorized_mean)
        except:
            comment_vectorized_mean = list(np.zeros(100))

        temp_row = np.asarray(comment_vectorized_mean)

        final_data.append(temp_row)

    X = np.asarray(final_data)
    print('Conversion to array complete')
    print('Clustering Comments')

    #Perform clustering
    clf = KMeans(n_clusters=2,n_jobs=-1,max_iter=50000,random_state=1)
    clf.fit(X)
    print('Clustering complete')

    joblib.dump(clf, '/Users/stephanie/PycharmProjects/TwitterBot/venv/models/gensim/results/cluster_news.pkl')
    print('Pickle file saved')

    #Put the cluster label in original dataframe beside CLASS label for comparison and save the csv file
    comment_label = clf.labels_
    comment_cluster_df = pd.DataFrame(original_df)
    comment_cluster_df['comment_label'] = np.nan
    comment_cluster_df['comment_label'] = comment_label
    print('Saving to csv')
    comment_cluster_df.to_csv('/Users/stephanie/PycharmProjects/TwitterBot/venv/models/gensim/results/comment_output.csv', index=False)

def clusterComcastTweets():
    '''Hide run tie warnings'''
    warnings.filterwarnings("ignore")

    '''load gensim model'''
    fname = "/Users/stephanie/PycharmProjects/TwitterBot/venv/models/gensim/text8_gensim"
    model = gensim.models.Word2Vec.load(fname)
    print("Gensim model has been loaded")

    '''reading from test data'''
    df = pd.read_csv('/Users/stephanie/PycharmProjects/TwitterBot/venv/clustering_data/comcast_tweets.csv',
                     encoding="latin-1")

    silhouette_avg_list =[]

    dfList = list(df['tweet_text'])
    length = 0
    for data in dfList:
        length += len(str(data))

    length = length / len(dfList)

    original_df = pd.DataFrame(df)


    '''Preparing data in correct format for clustering'''
    final_data = []

    for i, row in df.iterrows():
        comment_vectorized = []
        comment = row['tweet_text']
        comment_all_words = str(comment).split(sep=" ")

        for comment_w in comment_all_words:
            try:
                comment_vectorized.append(list(model[comment_w]))
            except Exception as e:
                print(e)

        try:
            comment_vectorized = np.asarray(comment_vectorized)
            comment_vectorized_mean = list(np.mean(comment_vectorized, axis=0))
        except Exception as e:
            comment_vectorized_mean = list(np.zeros(100))

        try:
            len(comment_vectorized_mean)
        except:
            comment_vectorized_mean = list(np.zeros(100))

        temp_row = np.asarray(comment_vectorized_mean)

        final_data.append(temp_row)

    X = np.asarray(final_data)
    print('Conversion to array complete')
    print('Clustering Comments')

    #n_clusters = 100
    random_state = 10

    for n_clusters in range(2,11):
        # Create a subplot with 1 row and 2 columns
        fig, (ax1, ax2) = plt.subplots(1, 2)
        fig.set_size_inches(18, 7)

        # The 1st subplot is the silhouette plot
        # The silhouette coefficient can range from -1, 1
        ax1.set_xlim([-1, 1])
        # The (n_clusters+1)*10 is for inserting blank space between silhouette
        # plots of individual clusters, to demarcate them clearly.
        ax1.set_ylim([0, len(X) + (n_clusters + 1) * 10])

        # Perform clustering
        clf = KMeans(n_clusters=n_clusters, max_iter=50000, random_state=random_state)
        clf.fit(X)
        y_kmeans = clf.predict(X)
        print('Clustering complete')

        silhouette_avg = silhouette_score(X, y_kmeans)
        print("For n_clusters =", n_clusters,
              "The average silhouette_score is :", silhouette_avg)

        silhouette_avg_list.append("For n_clusters = "+str(n_clusters)+
              " The average silhouette_score is : "+str(silhouette_avg))

        # Compute the silhouette scores for each sample
        sample_silhouette_values = silhouette_samples(X, y_kmeans)

        # joblib.dump(clf, '/Users/stephanie/PycharmProjects/TwitterBot/venv/models/gensim/results/cluster_+'+str(datetime.now())+'.pkl')
        # print('Pickle file saved')

        # Put the cluster label in original dataframe beside CLASS label for comparison and save the csv file
        comment_label = clf.labels_
        comment_cluster_df = pd.DataFrame(original_df)
        comment_cluster_df['cluster_label'] = np.nan
        comment_cluster_df['cluster_label'] = comment_label
        print('Saving to csv')
        comment_cluster_df.to_csv(
            '/Users/stephanie/PycharmProjects/TwitterBot/venv/clustering_data/comcast_tweet_output/comcast_output_clusters_'+str(n_clusters)+'_random_state_'+str(random_state)
            +'.csv', index=False)

        y_lower = 10
        for i in range(n_clusters):
            # Aggregate the silhouette scores for samples belonging to
            # cluster i, and sort them
            ith_cluster_silhouette_values = \
                sample_silhouette_values[y_kmeans == i]

            ith_cluster_silhouette_values.sort()

            size_cluster_i = ith_cluster_silhouette_values.shape[0]
            y_upper = y_lower + size_cluster_i

            color = cm.nipy_spectral(float(i) / n_clusters)
            ax1.fill_betweenx(np.arange(y_lower, y_upper),
                              0, ith_cluster_silhouette_values,
                              facecolor=color, edgecolor=color, alpha=0.7)

            # Label the silhouette plots with their cluster numbers at the middle
            ax1.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))

            # Compute the new y_lower for next plot
            y_lower = y_upper + 10  # 10 for the 0 samples

        ax1.set_title("The silhouette plot for the various clusters.")
        ax1.set_xlabel("The silhouette coefficient values")
        ax1.set_ylabel("Cluster label")

        # The vertical line for average silhouette score of all the values
        ax1.axvline(x=silhouette_avg, color="red", linestyle="--")

        ax1.set_yticks([])  # Clear the yaxis labels / ticks
        ax1.set_xticks([-0.1, 0, 0.2, 0.4, 0.6, 0.8, 1])

        # 2nd Plot showing the actual clusters formed
        colors = cm.nipy_spectral(y_kmeans.astype(float) / n_clusters)
        ax2.scatter(X[:, 0], X[:, 1], marker='.', s=30, lw=0, alpha=0.7,
                    c=colors, edgecolor='k')

        # Labeling the clusters
        centers = clf.cluster_centers_
        # Draw white circles at cluster centers
        ax2.scatter(centers[:, 0], centers[:, 1], marker='o',
                    c="white", alpha=1, s=200, edgecolor='k')

        for i, c in enumerate(centers):
            ax2.scatter(c[0], c[1], marker='$%d$' % i, alpha=1,
                        s=50, edgecolor='k')

        ax2.set_title("The visualization of the clustered data.")
        ax2.set_xlabel("Feature space for the 1st feature")
        ax2.set_ylabel("Feature space for the 2nd feature")

        plt.suptitle(("Silhouette analysis for KMeans clustering on sample data "
                      "with n_clusters = %d" % n_clusters),
                     fontsize=14, fontweight='bold')

        # plt.scatter(X[:, 0], X[:, 1], c=y_kmeans, s=50, cmap='viridis')

        # centers = clf.cluster_centers_
        # plt.scatter(centers[:, 0], centers[:, 1], c='black', s=200, alpha=0.5)

        #plt.show()
        plt.savefig('/Users/stephanie/PycharmProjects/TwitterBot/venv/clustering_data/comcast_tweet_output/comcast_output_clusters_'+str(n_clusters)+'_random_state_'+str(random_state)
            +'.png')

    with open('/Users/stephanie/PycharmProjects/TwitterBot/venv/clustering_data/comcast_tweet_output/result_clusters_'+str(n_clusters)+'_random_state_'+str(random_state)
            +'.txt','w') as result_file:
        for sil in silhouette_avg_list:
            result_file.write(str(sil))
            result_file.write("\n")

def clusterComcastLinesGMM():
    '''Hide run tie warnings'''
    warnings.filterwarnings("ignore")

    distortions = []

    '''load gensim model'''
    fname = "/Users/stephanie/PycharmProjects/TwitterBot/venv/models/gensim/text8_gensim"
    model = gensim.models.Word2Vec.load(fname)
    print("Gensim model has been loaded")

    '''reading from test data'''
    df = pd.read_csv('/Users/stephanie/PycharmProjects/TwitterBot/venv/clustering_data/comcast_line_data.csv',
                     encoding="latin-1")

    silhouette_avg_list = []

    dfList = list(df['content'])
    length = 0
    for data in dfList:
        length += len(str(data))

    length = length / len(dfList)

    original_df = pd.DataFrame(df)
    df = df.drop(['doc_id'], axis=1)

    '''Preparing data in correct format for clustering'''
    final_data = []

    for i, row in df.iterrows():
        comment_vectorized = []
        comment = row['content']
        comment_all_words = str(comment).split(sep=" ")

        for comment_w in comment_all_words:
            try:
                comment_vectorized.append(list(model[comment_w]))
            except Exception as e:
                print(e)

        try:
            comment_vectorized = np.asarray(comment_vectorized)
            comment_vectorized_mean = list(np.mean(comment_vectorized, axis=0))
        except Exception as e:
            comment_vectorized_mean = list(np.zeros(100))

        try:
            len(comment_vectorized_mean)
        except:
            comment_vectorized_mean = list(np.zeros(100))

        temp_row = np.asarray(comment_vectorized_mean)

        final_data.append(temp_row)

    X = np.asarray(final_data)
    print('Conversion to array complete')
    print('Clustering Comments')

    # n_clusters = 100
    random_state = 10

    for n_clusters in range(2, 31):

        # Perform clustering
        gmm = GaussianMixture(n_components=n_clusters).fit(X)
        labels = gmm.predict(X)
        plt.scatter(X[:, 0], X[:, 1], c=labels, s=40, cmap='viridis');
        probs = gmm.predict_proba(X)
        #print(probs[:5].round(3))
        size = 50 * probs.max(1) ** 2  # square emphasizes differences
        plt.scatter(X[:, 0], X[:, 1], c=labels, cmap='viridis', s=size);
        plt.savefig(
            '/Users/stephanie/PycharmProjects/TwitterBot/venv/clustering_data/comcast_line_gmm_op/comcast_output_clusters_' + str(
                n_clusters)+ '.png')

def clusterComcastArticlesByLines():
    '''Hide run tie warnings'''
    warnings.filterwarnings("ignore")

    output_path = '/Users/stephanie/PycharmProjects/TwitterBot/venv/clustering_data/comcast_2019_line_output/'

    distortions = []

    '''load gensim model'''
    fname = "/Users/stephanie/PycharmProjects/TwitterBot/venv/models/gensim/text8_150_gensim"
    model = gensim.models.Word2Vec.load(fname)
    print("Gensim model has been loaded")

    '''reading from test data'''
    df = pd.read_csv('/Users/stephanie/PycharmProjects/TwitterBot/venv/clustering_data/comcast_2019_line_data.csv',
                     encoding="latin-1")

    silhouette_avg_list =[]

    dfList = list(df['content'])
    length = 0
    for data in dfList:
        length += len(str(data))

    length = length / len(dfList)

    original_df = pd.DataFrame(df)
    df = df.drop(['doc_id'], axis=1)

    '''Preparing data in correct format for clustering'''
    final_data = []

    for i, row in df.iterrows():
        comment_vectorized = []
        comment = row['content']
        comment_all_words = str(comment).split(sep=" ")

        for comment_w in comment_all_words:
            try:
                comment_vectorized.append(list(model[comment_w]))
            except Exception as e:
                print(e)

        try:
            comment_vectorized = np.asarray(comment_vectorized)
            comment_vectorized_mean = list(np.mean(comment_vectorized, axis=0))
        except Exception as e:
            comment_vectorized_mean = list(np.zeros(150))

        try:
            len(comment_vectorized_mean)
        except:
            comment_vectorized_mean = list(np.zeros(150))

        temp_row = np.asarray(comment_vectorized_mean)

        final_data.append(temp_row)

    X = np.asarray(final_data)
    print('Conversion to array complete')
    print('Clustering Comments')

    #n_clusters = 100
    random_state = 10

    for n_clusters in range(2,21):
        # Create a subplot with 1 row and 2 columns
        fig, (ax1, ax2) = plt.subplots(1, 2)
        fig.set_size_inches(18, 7)

        # The 1st subplot is the silhouette plot
        # The silhouette coefficient can range from -1, 1
        ax1.set_xlim([-1, 1])
        # The (n_clusters+1)*10 is for inserting blank space between silhouette
        # plots of individual clusters, to demarcate them clearly.
        ax1.set_ylim([0, len(X) + (n_clusters + 1) * 10])

        # Perform clustering
        clf = KMeans(n_clusters=n_clusters, max_iter=50000, random_state=random_state)
        clf.fit(X)
        y_kmeans = clf.predict(X)

        distortions.append(sum(np.min(cdist(X, clf.cluster_centers_, 'euclidean'), axis=1)) / X.shape[0])
        print('Clustering complete')

        silhouette_avg = silhouette_score(X, y_kmeans)
        print("For n_clusters =", n_clusters,
              "The average silhouette_score is :", silhouette_avg)

        silhouette_avg_list.append("For n_clusters = "+str(n_clusters)+
              " The average silhouette_score is : "+str(silhouette_avg))

        # Compute the silhouette scores for each sample
        sample_silhouette_values = silhouette_samples(X, y_kmeans)

        # joblib.dump(clf, '/Users/stephanie/PycharmProjects/TwitterBot/venv/models/gensim/results/cluster_+'+str(datetime.now())+'.pkl')
        # print('Pickle file saved')

        # Put the cluster label in original dataframe beside CLASS label for comparison and save the csv file
        comment_label = clf.labels_
        comment_cluster_df = pd.DataFrame(original_df)
        comment_cluster_df['cluster_label'] = np.nan
        comment_cluster_df['cluster_label'] = comment_label
        print('Saving to csv')
        comment_cluster_df.to_csv(
            output_path+'comcast_output_clusters_'+str(n_clusters)+'_random_state_'+str(random_state)
            +'.csv', index=False)

        y_lower = 10
        for i in range(n_clusters):
            # Aggregate the silhouette scores for samples belonging to
            # cluster i, and sort them
            ith_cluster_silhouette_values = \
                sample_silhouette_values[y_kmeans == i]

            ith_cluster_silhouette_values.sort()

            size_cluster_i = ith_cluster_silhouette_values.shape[0]
            y_upper = y_lower + size_cluster_i

            color = cm.nipy_spectral(float(i) / n_clusters)
            ax1.fill_betweenx(np.arange(y_lower, y_upper),
                              0, ith_cluster_silhouette_values,
                              facecolor=color, edgecolor=color, alpha=0.7)

            # Label the silhouette plots with their cluster numbers at the middle
            ax1.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))

            # Compute the new y_lower for next plot
            y_lower = y_upper + 10  # 10 for the 0 samples

        ax1.set_title("The silhouette plot for the various clusters.")
        ax1.set_xlabel("The silhouette coefficient values")
        ax1.set_ylabel("Cluster label")

        # The vertical line for average silhouette score of all the values
        ax1.axvline(x=silhouette_avg, color="red", linestyle="--")

        ax1.set_yticks([])  # Clear the yaxis labels / ticks
        ax1.set_xticks([-0.1, 0, 0.2, 0.4, 0.6, 0.8, 1])

        # 2nd Plot showing the actual clusters formed
        colors = cm.nipy_spectral(y_kmeans.astype(float) / n_clusters)
        ax2.scatter(X[:, 0], X[:, 1], marker='.', s=30, lw=0, alpha=0.7,
                    c=colors, edgecolor='k')

        # Labeling the clusters
        centers = clf.cluster_centers_
        # Draw white circles at cluster centers
        ax2.scatter(centers[:, 0], centers[:, 1], marker='o',
                    c="white", alpha=1, s=200, edgecolor='k')

        for i, c in enumerate(centers):
            ax2.scatter(c[0], c[1], marker='$%d$' % i, alpha=1,
                        s=50, edgecolor='k')

        ax2.set_title("The visualization of the clustered data.")
        ax2.set_xlabel("Feature space for the 1st feature")
        ax2.set_ylabel("Feature space for the 2nd feature")

        plt.suptitle(("Silhouette analysis for KMeans clustering on sample data "
                      "with n_clusters = %d" % n_clusters),
                     fontsize=14, fontweight='bold')

        # plt.scatter(X[:, 0], X[:, 1], c=y_kmeans, s=50, cmap='viridis')

        # centers = clf.cluster_centers_
        # plt.scatter(centers[:, 0], centers[:, 1], c='black', s=200, alpha=0.5)

        #plt.show()
        plt.savefig(output_path+'comcast_output_clusters_'+str(n_clusters)+'_random_state_'+str(random_state)
            +'.png')

    with open(output_path+'result_clusters_'+str(n_clusters)+'_random_state_'+str(random_state)
            +'.txt','w') as result_file:
        for sil in silhouette_avg_list:
            result_file.write(str(sil))
            result_file.write("\n")

    # Plot the elbow
    plt.clf()
    plt.plot(range(2,21), distortions, 'bx-')
    plt.xlabel('k')
    plt.ylabel('Distortion')
    plt.title('The Elbow Method showing the optimal k')
    plt.savefig(
        output_path+'elbow_plot__' + str(
            n_clusters) + '_random_state_' + str(random_state)
        + '.png')

def performSentimentAnalysis():
    '''reading from test data'''
    df = pd.read_csv('/Users/stephanie/PycharmProjects/TwitterBot/venv/clustering_data/comcast_2019_line_data.csv',
                     encoding="latin-1")

    field_names = ['content','doc_id','result']

    with open('/Users/stephanie/PycharmProjects/TwitterBot/venv/sentiment_analysis_results/comcast_2019_results'
        + '.csv','w') as csv_file:
        writer = csv.DictWriter(csv_file,fieldnames=field_names)

        writer.writeheader()
        for i, row in df.iterrows():
            comment = row['content']
            # sent = get_vader_sentiment(str(comment))
            sent, result = sentiment_scores(str(comment))
            writer.writerow({field_names[0]: row['content'], field_names[1]: row['doc_id'],field_names[2]: result})


    # for i, row in df.iterrows():
    #     comment = row['content']
    #     #sent = get_vader_sentiment(str(comment))
    #     sent,result = sentiment_scores(str(comment))
    #     # result_df['neg'] = float(sent['neg'])
    #     # result_df['neu'] = float(sent['neu'])
    #     # result_df['pos'] = float(sent['pos'])
    #     # result_df['compound'] = float(sent['compound'])
    #     result_df['result'] = result
    #     result_df.append({'result':result},ignore_index=True)
    #
    # print('Saving to csv')
    # result_df.to_csv(
    #     '/Users/stephanie/PycharmProjects/TwitterBot/venv/sentiment_analysis_results/comcast_results'
    #     + '.csv', index=False)

def performAmazonSentimentAnalysis():
    '''reading from test data'''
    df = pd.read_csv('/Users/stephanie/PycharmProjects/TwitterBot/venv/clustering_data/amazon_reviews.csv',
                     encoding="latin-1")

    result_df = pd.DataFrame(df)
    field_names = ['content','rating','result']

    with open('/Users/stephanie/PycharmProjects/TwitterBot/venv/sentiment_analysis_results/amazon_reviews_results'
        + '.csv','w') as csv_file:
        writer = csv.DictWriter(csv_file,fieldnames=field_names)

        writer.writeheader()
        for i, row in df.iterrows():
            comment = row['content']
            # sent = get_vader_sentiment(str(comment))
            sent, result = sentiment_scores(str(comment))
            writer.writerow({field_names[0]: row['content'], field_names[1]: row['rating'],field_names[2]: result})


    # for i, row in df.iterrows():
    #     comment = row['content']
    #     #sent = get_vader_sentiment(str(comment))
    #     sent,result = sentiment_scores(str(comment))
    #     # result_df['neg'] = float(sent['neg'])
    #     # result_df['neu'] = float(sent['neu'])
    #     # result_df['pos'] = float(sent['pos'])
    #     # result_df['compound'] = float(sent['compound'])
    #     result_df['result'] = result
    #     result_df.append({'result':result},ignore_index=True)
    #
    # print('Saving to csv')
    # result_df.to_csv(
    #     '/Users/stephanie/PycharmProjects/TwitterBot/venv/sentiment_analysis_results/comcast_results'
    #     + '.csv', index=False)

def classifySentence():
    '''reading from test data'''
    df = pd.read_csv('/Users/stephanie/PycharmProjects/TwitterBot/venv/clustering_data/comcast_sentiment_results.csv',
                     encoding="latin-1")
    field_names = ['content', 'doc_id', 'result','business_category']

    with open('/Users/stephanie/PycharmProjects/TwitterBot/venv/sentiment_analysis_results/comcast_cat_results'
              + '.csv', 'w') as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=field_names)

        writer.writeheader()
        for i, row in df.iterrows():
            comment = row['content']
            # sent = get_vader_sentiment(str(comment))
            result = classifyBasedOnKeyword(str(comment))
            writer.writerow({field_names[0]: row['content'], field_names[1]: row['doc_id'], field_names[2]: row['result'], field_names[3]: ','.join(result)})

def getFrequentData():
    tObj = ta.tweetanalysis()

    df = pd.read_csv('/Users/stephanie/PycharmProjects/TwitterBot/venv/sentiment_analysis_results/comcast_2019_results.csv',
                     encoding="latin-1")

    final_words_list = []

    for i, row in df.iterrows():
        comment = row['content']
        final_words_list += tObj.frequencyDistOfText(str(comment).lower(), 'sdfwe')

    frequent_hashtags, frequent_users, frequent_words = tObj.findRelatedTopics(final_words_list)

    print("Frequently used words")
    print(frequent_words)


def getFrequentNgrams():
    tObj = ta.tweetanalysis()

    df = pd.read_csv('/Users/stephanie/PycharmProjects/TwitterBot/venv/sentiment_analysis_results/comcast_2019_results.csv',
                     encoding="latin-1")

    final_words_list = []

    for i, row in df.iterrows():
        comment = row['content']
        final_words_list.append(str(comment))


    print("Frequently used N-grams")
    tObj.findFrequentNgrams(final_words_list)

def findNouns():
    df = pd.read_csv('/Users/stephanie/PycharmProjects/TwitterBot/venv/sentiment_analysis_results/comcast_cat_results.csv',
                     encoding="latin-1")

    #region Finding Nouns
    # for i,row in df.iterrows():
    #     if 'merger' in str(row['business_category']):
    #         content = str(row['content'])
    #         blob = TextBlob(content)
    #         print("Phrase")
    #         print(str(row["content"]))
    #         print("Nouns")
    #         print(blob.noun_phrases)
    #endregion

    #region Finding Proper Nouns
    for i,row in df.iterrows():
        if 'revenue' in str(row['business_category']):
            proper_nouns =[]
            content = str(row['content'])
            for word, pos in nltk.pos_tag(nltk.word_tokenize(str(content))):
                if (pos == 'NNP'):
                    proper_nouns.append(word)
            print("Doc ID")
            print(str(row["doc_id"]))
            print("Proper Nouns")
            print(proper_nouns)
    #endregion

def SegregateNewsArticles():
    '''reading from test data'''
    df = pd.read_csv('/Users/stephanie/PycharmProjects/TwitterBot/venv/clustering_data/news_articles.csv',
                     encoding="latin-1")
    field_names = ['id', 'title', 'url']

    company_names = ['Comcast','Xfinity','NBCUniversal']

    with open('/Users/stephanie/PycharmProjects/TwitterBot/venv/sentiment_analysis_results/comcast_news_primary'
              + '.csv', 'w') as csv_file:
        primary_writer = csv.DictWriter(csv_file, fieldnames=field_names)

        primary_writer.writeheader()
        with open('/Users/stephanie/PycharmProjects/TwitterBot/venv/sentiment_analysis_results/comcast_news_secondary'
                  + '.csv', 'w') as csv_file:
            secondary_writer = csv.DictWriter(csv_file, fieldnames=field_names)
            secondary_writer.writeheader()

            for i, row in df.iterrows():
                title = str(row['title'])
                url = str(row['url'])

                if any(company_name in title for company_name in company_names) or any(company_name in url for company_name in company_names):
                    primary_writer.writerow(
                        {field_names[0]: row['id'], field_names[1]: row['title'], field_names[2]: row['url']})
                else:
                    secondary_writer.writerow(
                        {field_names[0]: row['id'], field_names[1]: row['title'], field_names[2]: row['url']})



def AmazonJsonToCSV():
    print('reading data')
    field_names = ['content','rating']
    with open('/Users/stephanie/PycharmProjects/TwitterBot/venv/clustering_data/echo_3_data.json') as json_file:
        data = json.load(json_file)
        with open('/Users/stephanie/PycharmProjects/TwitterBot/venv/clustering_data/amazon_reviews'
                  + '.csv', 'w') as csv_file:
            primary_writer = csv.DictWriter(csv_file, fieldnames=field_names)
            primary_writer.writeheader()
            for data_row in data:
                try:
                    reviews = data_row['reviews']
                    for review in reviews:
                        print(str(review['review_text']))
                        primary_writer.writerow(
                            {field_names[0]: str(review['review_text']), field_names[1]: str(review['review_rating'])})
                except Exception as e:
                    continue




performAmazonSentimentAnalysis()
















