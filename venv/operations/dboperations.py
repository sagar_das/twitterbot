import sqlite3
from datetime import datetime
class dboperations:
    """
    This class is responsible for all sql lite database operations
    """
    sqldb = None
    def __init__(self):
        """
        The constructor opens the database connection
        """
        try:
            self.sqldb = sqlite3.connect('/Users/stephanie/PycharmProjects/TwitterBot/venv/cxmarket.db',check_same_thread=False)
            print('DB created/opened successfully')
        except Exception as e: print(e)
    def createTable(self):
        """
                Creates all the tables
                """
        try:
            cursor = self.sqldb.execute("SELECT name FROM sqlite_master WHERE type='table' AND name= 'FOLLOWERS' ")
            row_count = 0
            for row in cursor:
                row_count+=1
            if row_count == 0:
                self.sqldb.execute('''
                CREATE TABLE FOLLOWERS
                (ID     INT     PRIMARY KEY     NOT NULL,
                NAME    TEXT    NULL,
                SCREEN_NAME    TEXT    NOT NULL,
                LOCATION    TEXT    NULL,
                DESCRIPTION    TEXT    NULL,
                FOLLOWERS_COUNT INT NULL,
                WELCOME_MSG_SENT INT NOT NULL,
                HAS_UNFOLLOWED INT NULL)''')

            cursor = self.sqldb.execute("SELECT name FROM sqlite_master WHERE type='table' AND name= 'TWEETS' ")
            row_count = 0
            for row in cursor:
                row_count += 1
            if row_count == 0:
                self.sqldb.execute('''
                            CREATE TABLE TWEETS
                            (ID     INT     PRIMARY KEY     NOT NULL,
                            TWEET_TEXT    TEXT    NULL,
                            HASH_TAG TEXT NULL,
                            EXPANDED_URL TEXT NULL,
                            FAVOURITES_COUNT INT NULL,
                            RETWEET_COUNT INT NULL,
                            USER_ID INT NULL,
                            USER_NAME    TEXT    NULL,
                            USER_SCREEN_NAME    TEXT    NOT NULL,
                            USER_LOCATION    TEXT    NULL,
                            USER_DESCRIPTION    TEXT    NULL,
                            USER_FOLLOWERS_COUNT INT NULL,
                            SELECTED INT NULL,
                            INSRTD_TMP_STMP TEXT NOT NULL,
                            REPLY_FETCH  INT  DEFAULT (0))''')

            cursor = self.sqldb.execute("SELECT name FROM sqlite_master WHERE type='table' AND name= 'USER_TIMELINE' ")
            row_count = 0
            for row in cursor:
                row_count += 1
            if row_count == 0:
                self.sqldb.execute('''
                                        CREATE TABLE USER_TIMELINE
                                        (ID     INT     PRIMARY KEY     NOT NULL,
                                        TWEET_TEXT    TEXT    NULL,
                                        EXPANDED_URL TEXT NULL,
                                        MEDIA_URL TEXT NULL,
                                        FAVOURITES_COUNT INT NULL,
                                        RETWEET_COUNT INT NULL,
                                        USER_SCREEN_NAME    TEXT    NOT NULL,
                                        INSRTD_TMP_STMP TEXT NOT NULL)''')

            cursor = self.sqldb.execute("SELECT name FROM sqlite_master WHERE type='table' AND name= 'GNEWS' ")
            row_count = 0
            for row in cursor:
                row_count += 1
            if row_count == 0:
                self.sqldb.execute('''
                                                    CREATE TABLE GNEWS
                                                    (ID     INT     PRIMARY KEY     NOT NULL,
                                                    AUTHOR    TEXT    NULL,
                                                    TITLE    TEXT    NULL,
                                                    CONTENT TEXT NULL,
                                                    DESCRIPTION TEXT NULL,
                                                    PUBLISHED_AT TEXT NULL,
                                                    SOURCE TEXT NULL,
                                                    URL    TEXT    NOT NULL,
                                                    URL_TO_IMAGE TEXT NOT NULL,
                                                    INSRTD_TMP_STMP TEXT NOT NULL)''')

            cursor = self.sqldb.execute("SELECT name FROM sqlite_master WHERE type='table' AND name= 'REPLIES' ")
            row_count = 0
            for row in cursor:
                row_count += 1
            if row_count == 0:
                self.sqldb.execute('''
                                        CREATE TABLE REPLIES
                                        (ID     INT     PRIMARY KEY     NOT NULL,
                                        PARENT_TWEET_ID INT,
                                        TWEET_TEXT    TEXT    NULL,
                                        EXPANDED_URL TEXT NULL,
                                        FAVOURITES_COUNT INT NULL,
                                        RETWEET_COUNT INT NULL,
                                        USER_ID INT NULL,
                                        USER_NAME    TEXT    NULL,
                                        USER_SCREEN_NAME    TEXT    NOT NULL,
                                        USER_LOCATION    TEXT    NULL,
                                        USER_DESCRIPTION    TEXT    NULL,
                                        USER_FOLLOWERS_COUNT INT NULL,
                                        SELECTED INT NULL)''')

            cursor = self.sqldb.execute("SELECT name FROM sqlite_master WHERE type='table' AND name= 'MENTION_TWEETS' ")
            row_count = 0
            for row in cursor:
                row_count += 1
            if row_count == 0:
                self.sqldb.execute('''
                                        CREATE TABLE MENTION_TWEETS
                                        (ID     INT     PRIMARY KEY     NOT NULL,
                                        TWEET_TEXT    TEXT    NULL,
                                        FAVOURITES_COUNT INT NULL,
                                        RETWEET_COUNT INT NULL,
                                        USER_NAME    TEXT    NULL,
                                        USER_SCREEN_NAME    TEXT    NOT NULL,
                                        USER_LOCATION    TEXT    NULL,
                                        USER_DESCRIPTION    TEXT    NULL,
                                        USER_FOLLOWERS_COUNT INT NULL,
                                        IS_REPLIED INT NOT NULL)''')


            cursor = self.sqldb.execute("SELECT name FROM sqlite_master WHERE type='table' AND name= 'BLOG_POSTS' ")
            row_count = 0
            for row in cursor:
                row_count += 1
            if row_count == 0:
                self.sqldb.execute('''
                                                    CREATE TABLE BLOG_POSTS
                                                    (ID     INT     PRIMARY KEY     NOT NULL,
                                                    BLOG_TEXT    TEXT    NULL,
                                                    BLOG_SOURCE TEXT NULL,
                                                    TWEET_COUNT INT NULL,
                                                    INSRT_TMP_STMP INT NULL,
                                                    UPDT_TMP_STMP INT NULL)''')


        except Exception as e: print(e)


    def insertIntoTable(self,tablename,values):
        """
        Inserts values into table
        :param tablename: Table name as string
        :param values: Column names and values to be inserted represented as dictionary
        :return: Returns insertion result
        """
        result = 'fail'
        try:
            if tablename == 'FOLLOWERS':
                name = (values['name'].encode('ascii','ignore')).decode('utf-8').rstrip(' ')

                self.sqldb.execute("INSERT INTO FOLLOWERS (ID,NAME,SCREEN_NAME,LOCATION,DESCRIPTION,FOLLOWERS_COUNT,WELCOME_MSG_SENT,"
                                   "HAS_UNFOLLOWED) "
                      "VALUES ("+str(values['id'])+",'"+name+"','"+values['screen_name']+"','"+values['location']+""
                            "','"+values['description']+"',"+str(values['followers_count'])+",0,0 )")
                self.sqldb.commit()
                result = 'success'
            elif tablename == 'TWEETS':
                self.sqldb.execute("INSERT INTO TWEETS (ID,TWEET_TEXT,HASH_TAG,FAVOURITES_COUNT,RETWEET_COUNT,EXPANDED_URL,USER_ID,USER_NAME,USER_SCREEN_NAME,USER_LOCATION"
                                   ",USER_DESCRIPTION,USER_FOLLOWERS_COUNT,INSRTD_TMP_STMP) \
                                  VALUES (" + str(values['id']) + ",'" + values['text'] + "','" + values['hash_tag'] + "',"+ str(values['favourite_count']) + "," + str(values['retweet_count']) +",'" + str(values['expanded_url'])+"',"+str(values['user_id'])+",'" + values['user_name'] +
                                   "','" + values['user_screen_name'] +"','" + values['user_location'] +"','" + values['user_description'] +
                                   "'," + str(values['user_followers_count']) +",'"+str(datetime.now())+"' )")

                self.sqldb.commit()
                result = 'success'
            elif tablename == 'GNEWS':
                self.sqldb.execute("INSERT INTO GNEWS (ID,AUTHOR,TITLE,CONTENT,DESCRIPTION,PUBLISHED_AT,SOURCE,URL,URL_TO_IMAGE,INSRTD_TMP_STMP) \
                                  VALUES (" + str(values['id']) + ",'" + values['author'] + "','" + values['title'] + "','"+ values['content'] + "','" + values['description'] +"','" + values['published_at']+"','"+values['source']+"','" + values['url'] +
                                   "','" + values['url_to_image'] +"','" +str(datetime.now())+"' )")

                self.sqldb.commit()
                result = 'success'
            elif tablename == 'USER_TIMELINE':
                self.sqldb.execute("INSERT INTO USER_TIMELINE (ID,TWEET_TEXT,FAVOURITES_COUNT,RETWEET_COUNT,EXPANDED_URL,USER_SCREEN_NAME,INSRTD_TMP_STMP,MEDIA_URL) \
                                  VALUES (" + str(values['id']) + ",'" + values['text'] + "'," + str(values['favourite_count']) + "," + str(values['retweet_count']) +",'" + str(values['expanded_url'])+"','"+ values['user_screen_name'] +"','" +
                                   str(datetime.now())+"','" + str(values['media_url'])+"' )")

                self.sqldb.commit()
                result = 'success'

            elif tablename == 'REPLIES':
                self.sqldb.execute("INSERT INTO REPLIES (ID,PARENT_TWEET_ID,TWEET_TEXT,FAVOURITES_COUNT,RETWEET_COUNT,EXPANDED_URL,USER_ID,USER_NAME,USER_SCREEN_NAME,USER_LOCATION"
                                   ",USER_DESCRIPTION,USER_FOLLOWERS_COUNT) \
                                  VALUES (" + str(values['id']) + ","+str(values['parent_tweet_id']) + ",'" + values['text'] + "'," + str(values['favourite_count']) + "," + str(values['retweet_count']) +",'" + str(values['expanded_url'])+"',"+str(values['user_id'])+",'" + values['user_name'] +
                                   "','" + values['user_screen_name'] +"','" + values['user_location'] +"','" + values['user_description'] +
                                   "'," + str(values['user_followers_count']) +" )")

                self.sqldb.commit()
                result = 'success'
        except Exception as e :
            print (e)



        return result


    def selectRawQuery(self,queyString):
        '''

        :param queyString:
        :return:
        '''
        cursor = None
        try:
            cursor = self.sqldb.execute(queyString)
        except Exception as e : print(e)

        return cursor

    def selectFromTable(self,tablename,selectionArgs=None):
        """
        Selects
        :param tablename: Table name as String
        :param values: selection args as dictionary
        :return: returns cursor
        """
        cursor = None
        try:
            if tablename == 'FOLLOWERS' or tablename == 'TWEETS' or tablename == 'REPLIES':
                if selectionArgs == None:
                    cursor = self.sqldb.execute('SELECT * FROM '+tablename+' ORDER BY ID ASC')
                else:
                    for key in selectionArgs:
                        whereColumnName = key
                        whereColumnVal = selectionArgs[key]
                    cursor = self.sqldb.execute("SELECT * FROM " +tablename+" WHERE "+whereColumnName+" = '"+str(whereColumnVal)+"' ORDER BY ID DESC")

        except Exception as e : print(e)

        return cursor

    def updateTable(self,tablename,setValues,updateArgs=None):
        """

        :param tablename: Tabvle name as String
        :param setValues: Column values to be set as dictionary
        :param updateArgs: Update args as dictionary
        :return: returns update status
        """

        result = 'fail'

        try:
            if updateArgs==None:
                pass
            else:
                setColumnName = None
                setColumnVal = None
                whereColumnName = None
                whereColumnVal = None

                for key in setValues:
                    setColumnName = key
                    setColumnVal = setValues[key]
                for key in updateArgs:
                    whereColumnName = key
                    whereColumnVal = updateArgs[key]

                self.sqldb.execute("UPDATE "+ tablename +" set "+ setColumnName +" = "+ str(setColumnVal) +" where " +
                                   whereColumnName +" = "+str(whereColumnVal))
                self.sqldb.commit()
                result = 'success'

        except Exception as e: print(e)


        return result

    def updateTableBlob(self,tablename,setValues,updateArgs=None):
        """

        :param tablename: Tabvle name as String
        :param setValues: Column values to be set as dictionary
        :param updateArgs: Update args as dictionary
        :return: returns update status
        """

        result = 'fail'

        try:
            if updateArgs==None:
                pass
            else:
                setColumnName = None
                setColumnVal = None
                whereColumnName = None
                whereColumnVal = None

                for key in setValues:
                    setColumnName = key
                    setColumnVal = setValues[key]
                for key in updateArgs:
                    whereColumnName = key
                    whereColumnVal = updateArgs[key]

                sql = '''UPDATE GNEWS
                        SET SCRAPED_DATA = ?
                        WHERE ID = ?;'''
                self.sqldb.execute(sql, [setColumnVal, whereColumnVal])


                self.sqldb.commit()
                result = 'success'

        except Exception as e: print(e)


        return result

    def deleteFromTable(self,tablename,deleteArgs=None):
        """

        :param tablename:
        :param deleteArgs:
        :return:
        """
        result = 'fail'

        whereColumnName = None
        whereColumnVal = None

        for key in deleteArgs:
            whereColumnName = key
            whereColumnVal = deleteArgs[key]

        try:
            self.sqldb.execute("DELETE FROM " +tablename+" WHERE "+whereColumnName+" = "+whereColumnVal)
            self.sqldb.commit()
            result = 'success'
        except Exception as e : print(e)

        return result


    def closeConn(self):
        self.sqldb.close()


