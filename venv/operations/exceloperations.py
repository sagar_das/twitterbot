import xlsxwriter
from datetime import datetime
import json
import os
from operations import dboperations as dbo
from datetime import date

class exceloperations:
    workbook = None
    userworkbook = None
    strategyworkbook = None
    def __init__(self):
        self.workbook = xlsxwriter.Workbook('/Users/stephanie/PycharmProjects/TwitterBot/venv/reports/daily_trending_tweets'+str(datetime.now())+'.xlsx')
        self.userworkbook = xlsxwriter.Workbook('/Users/stephanie/PycharmProjects/TwitterBot/venv/reports/daily_potential_users'+str(datetime.now())+'.xlsx')
        self.weeklyworkbook = xlsxwriter.Workbook('/Users/stephanie/PycharmProjects/TwitterBot/venv/reports/weekly_report'+str(datetime.now())+'.xlsx')
        self.strategyworkbook = xlsxwriter.Workbook('/Users/stephanie/PycharmProjects/TwitterBot/venv/reports/strategy_tweets'+str(datetime.now())+'.xlsx')

    def createTweetSheet(self,tweets_json):
        path = '/Users/stephanie/PycharmProjects/TwitterBot/venv/reports/'
        json_file_name = 'tweets_2019-04-12 15:44:15.020652.json'

        worksheet = self.workbook.add_worksheet("Tweets")

        #Writing headers
        row = 0
        column_names =['topic','tweet_url','tweet_text','tweeted_by','retweet_count','favourites_count','no_of_replies','postive_reply_percent','negative_reply_percent']
        for column in range(0,9):
            worksheet.write(row,column,column_names[column])

        for i in os.listdir(path):
            if '.DS_Store' in str(i) or 'user' in str(i) or '.xls' in str(i):
                continue
            with open(path+str(i)) as json_file:
                data = json.load(json_file)
                for p in data:
                    row+=1
                    for column in range(0,9):
                        worksheet.write(row, column, p[column_names[column]])

        self.workbook.close()

    def createStrategyTweetSheet(self,tweets_json):
        path = '/Users/stephanie/PycharmProjects/TwitterBot/venv/reports/'
        json_file_name = 'tweets_2019-04-12 15:44:15.020652.json'

        worksheet = self.strategyworkbook.add_worksheet("Strategy,Tips,Insights_Tweets")

        #Writing headers
        row = 0
        column_names =['tweet_id','tweet_url','tweet_text','tweet_hash_tag']
        for column in range(0,4):
            worksheet.write(row,column,column_names[column])

        for i in os.listdir(path):
            if '.DS_Store' in str(i) or 'user' in str(i) or '.xls' in str(i) or 'weekly' in str(i):
                continue
            with open(path+str(i)) as json_file:
                data = json.load(json_file)
                for p in data:
                    row+=1
                    for column in range(0,4):
                        worksheet.write(row, column, p[column_names[column]])

        self.strategyworkbook.close()

    def createUserSheet(self,tweets_json):
        path = '/Users/stephanie/PycharmProjects/TwitterBot/venv/reports/'
        json_file_name = 'tweets_2019-04-12 15:44:15.020652.json'

        worksheet = self.userworkbook.add_worksheet("Users")

        #Writing headers
        row = 0
        column_names =['user_name','user_screen_name','user_location','user_description','user_followers_count','hash_tag']
        for column in range(0,6):
            worksheet.write(row,column,column_names[column])

        for i in os.listdir(path):
            if '.DS_Store' in str(i) or 'tweet' in str(i):
                continue
            with open(path+str(i)) as json_file:
                data = json.load(json_file)
                for p in data:
                    row+=1
                    for column in range(0,6):
                        worksheet.write(row, column, p[column_names[column]])

        self.userworkbook.close()

    def createTopicSheet(self,topic_json):
        path = '/Users/stephanie/PycharmProjects/TwitterBot/venv/reports/'
        json_file_name = 'tweets_2019-04-12 15:44:15.020652.json'

        worksheet = self.weeklyworkbook.add_worksheet("Community analysis")

        # Writing headers
        row = 0
        column_names = ['community', 'frequently_mentioned_topics', 'frequently_mentioned_users', 'frequently_used_words']
        for column in range(0, 4):
            worksheet.write(row, column, column_names[column])

        for i in os.listdir(path):

            if '.DS_Store' in str(i) or 'user' in str(i) or 'tweet' in str(i) or '.xls' in str(i):
                continue
            if str(date.today()) not in str(i):
                continue
            with open(path + str(i)) as json_file:
                data = json.load(json_file)
                for p in data:
                    row += 1
                    for column in range(0, 4):
                        if column == 0:
                            worksheet.write(row, column, p['topic'])
                        elif column == 1:
                            hash_tag_list = p['frequent_hashtags']
                            hash_tags = ''
                            for hashtag in hash_tag_list[:5]:
                                hash_tags+=hashtag[0]+","

                            worksheet.write(row, column, hash_tags)
                        elif column == 2:
                            users_list = p['frequent_users']
                            users = ''
                            for user in users_list[:3]:
                                users+=user[0]+","

                            worksheet.write(row, column, users)
                        elif column == 3:
                            words_list = p['frequent_words']
                            words = ''
                            for word in words_list[:5]:
                                words+=word[0]+","

                            worksheet.write(row, column, words)


        self.weeklyworkbook.close()

    def createStrategyTweetSheet(self,tweets_json):
        path = '/Users/stephanie/PycharmProjects/TwitterBot/venv/reports/'


        worksheet = self.strategyworkbook.add_worksheet("Strategy,Tips,Insights_Tweets")

        #Writing headers
        row = 0
        column_names =['tweet_id','tweet_url','tweet_text']
        for column in range(0,3):
            worksheet.write(row,column,column_names[column])

        for i in os.listdir(path):
            if '.DS_Store' in str(i) or '.tsv' in str(i) or '.xls' in str(i) :
                continue

            if str(date.today()) not in str(i):
                continue

            with open(path+str(i)) as json_file:
                data = json.load(json_file)
                for p in data:
                    row+=1
                    for column in range(0,3):
                        worksheet.write(row, column, p[column_names[column]])

        self.strategyworkbook.close()





# ex = exceloperations()
# #ex.createTweetSheet(None)
# #ex.createUserSheet(None)
# #ex.createTopicSheet(None)
# db = dbo.dboperations()
# result = db.selectRawQuery("select distinct * from tweets where hash_tag = 'ecommerce' and tweet_text not like '%RT%'")
# result_list= []
#
# workbook = xlsxwriter.Workbook('/Users/stephanie/PycharmProjects/TwitterBot/venv/reports/daily_temp'+str(datetime.now())+'.xlsx')
# worksheet = workbook.add_worksheet("Tweets")
#
# row_count = 0
#
# for row in result:
#     result_list = str(row[1]).split(" ")
#     final_tweet = ""
#     for word in result_list:
#         if '#' in word or 'https' in word or '@' in word:
#             continue
#         final_tweet+=word+" "
#
#     worksheet.write(row_count,0,row[0])
#     worksheet.write(row_count, 1, final_tweet)
#     row_count+=1
#
# workbook.close()


