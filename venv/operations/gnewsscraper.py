from newsapi import NewsApiClient
import pprint
import json
from requests import get


class gnewsscraper:

    newsapi = None

    def __init__(self):
        api_key = None
        path = "/Users/stephanie/PycharmProjects/TwitterBot/venv/cred/"
        cred_file = "gnews_api_key.json"

        with open(path + cred_file) as json_file:
            data = json.load(json_file)
            api_key = data['api_key']

        # Init
        self.newsapi = NewsApiClient(api_key=api_key)

    def getDetailedArticles(self,query):

        all_articles = self.newsapi.get_everything(q=query,
                                              from_param='2019-04-01',
                                              to='2019-04-30',
                                              language='en',
                                              sort_by='relevancy'
                                              )

        # pp = pprint.PrettyPrinter(compact=True)
        # pp.pprint(all_articles)

        return all_articles

    def getTopHeadlines(self,query):
        top_headlines = self.newsapi.get_top_headlines(q=query,
                                                  sources='bbc-news,the-verge',
                                                  category='business',
                                                  language='en',
                                                  country='us')
        return top_headlines

    def scrapeArticleData(self,url):
        headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36'}
        for i in range(5):
            response = get(url, headers=headers, verify=False, timeout=30)
            if response.status_code == 404:
                return {"url": url, "error": "page not found"}
            if response.status_code != 200:
                continue

            # Removing the null bytes from the response.
            cleaned_response = response.text.replace('\x00', '')

        return cleaned_response





# # /v2/top-headlines






