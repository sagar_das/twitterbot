from operations import dboperations as dbo
from operator import itemgetter
from datetime import datetime
from datetime import date, timedelta
from langdetect import detect
import csv
class marketanalysis:
    db = None
    def __init__(self):
        self.db = dbo.dboperations()


    def getWeeklyTweetsByHashTag(self,hashTag):
        dateCond1 = str(date.today() - timedelta(7)) + ' 00:00:00.000000'
        dateCond2 = str(datetime.now())
        tweet_text_list = []
        select_query = "select distinct * from tweets where hash_tag = '"+hashTag+"' and tweet_text not like 'RT%' and insrtd_tmp_stmp between '"+dateCond1+"' and '"+dateCond2+"'"

        cursor = self.db.selectRawQuery(select_query)

        for row in cursor:
            tweet_text_list.append(str(row[1]))

        return tweet_text_list


    def getTrendingTweetsByHashTag(self,hashTag):
        dateCond1 = str(date.today() - timedelta(1)) + ' 00:00:00.000000'
        dateCond2 = str(datetime.now())
        select_query = "select distinct tweet_text from" \
                       " (select * from TWEETS where hash_tag = '"+hashTag+"' and insrtd_tmp_stmp between '"+dateCond1+"' and '"+dateCond2+"' order by retweet_count desc) " \
                                                                           "where tweet_text like 'RT%'"
        cursor = self.db.selectRawQuery(select_query)

        selected_tweets_list = []
        original_tweet_details = []

        for row in cursor:
            if str(detect(str(row[0]))) != 'en':
                print(str(row[0]))
                print('skipping')
                continue

            retweet_count = 0
            id = 0
            select_query = "select max(retweet_count) from tweets where tweet_text like '%"+str(row[0])+"%'"
            retweet_cursor = self.db.selectRawQuery(select_query)
            for retweet in retweet_cursor:
                retweet_count = retweet[0]

            select_query = "select id from tweets where tweet_text like '%"+str(row[0])+"%' order by insrtd_tmp_stmp desc limit 1"
            id_cursor = self.db.selectRawQuery(select_query)
            for id_row in id_cursor:
                id = id_row[0]


            tweet_dict = {'tweet_text':row[0],
                          'retweet_count':retweet_count,
                          'retweet_tweet_id':id}
            selected_tweets_list.append(tweet_dict)



        selected_tweets_list_sorted = sorted(selected_tweets_list, key=itemgetter('retweet_count'), reverse=True)

        return selected_tweets_list_sorted

    def getAndSaveDailyTweetsByHashTag(self,hashTagList):
        dateCond1 = str(date.today() - timedelta(30)) + ' 00:00:00.000000'
        dateCond2 = str(datetime.now())
        tweet_text_list = []

        with open('/Users/stephanie/PycharmProjects/TwitterBot/venv/reports/daily_context_tweets'+str(date.today())+'.tsv', 'w') as out_file:
            tsv_writer = csv.writer(out_file, delimiter='\t')
            tsv_writer.writerow(['tweet_id', 'tweet_text','tweet_hash_tag'])


        for hashTag in hashTagList:
            select_query = "select distinct * from tweets where hash_tag = '" + hashTag + "' and tweet_text not like 'RT%' and insrtd_tmp_stmp between '" + dateCond1 + "' and '" + dateCond2 + "'"

            cursor = self.db.selectRawQuery(select_query)

            with open('/Users/stephanie/PycharmProjects/TwitterBot/venv/reports/context_tweets'+str(date.today())+'.tsv', 'a') as out_file:
                tsv_writer = csv.writer(out_file, delimiter='\t')
                for row in cursor:

                    result_list = str(row[1]).split(" ")
                    final_tweet = ""
                    for word in result_list:
                        if '#' in word or 'https' in word or '@' in word:
                            continue
                        final_tweet += word + " "

                    tsv_writer.writerow([str(row[0]), final_tweet,hashTag])


    def getUserDetails(self,text,hashtag):
        select_query = "select user_name,user_screen_name,user_location,user_description,user_followers_count from TWEETS where tweet_text like '%"+text+"%'"
        cursor = self.db.selectRawQuery(select_query)

        users_list = []

        for row in cursor:
            if row[4] < 3000:
                continue
            user_dict = {'user_name': row[0],
                          'user_screen_name': row[1],
                          'user_location': row[2],
                        'user_description': row[3],
                        'user_followers_count': row[4],
                         'hash_tag': hashtag}

            if str(row[1]) not in [str(user['user_screen_name']) for user in users_list]:
                users_list.append(user_dict)





        return users_list

    def tempOerations(self):
        print(detect('今回は電話占いピュアリに舞い降りた美魔女、葛の葉先生のミステリアス＆キュートトークが満載です！'))
        print(detect('hello there what are you doing ?'))
























