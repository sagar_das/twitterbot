import json
import numpy as np
import tensorflow as tf
import tensorflow.keras.preprocessing.text as kpt
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.models import model_from_json
from datetime import datetime

class replysentimentanalysis:
    def __init__(self):
        pass
    def classifySentiment(self,evalSentence):
        tokenizer = Tokenizer(num_words=3000)
        labels = ['negative', 'positive']
        untrained_words = []
        with open('/Users/stephanie/PycharmProjects/TwitterBot/venv/models/word_dictionary.json', 'r') as dictionary_file:
            word_dictionary = json.load(dictionary_file)

            def convert_text_to_index_array(text):
                '''
                Checks that all the words in the input
                are registered in the dictionary before
                turning them into matrix
                :param text:
                :return:
                '''
                words = kpt.text_to_word_sequence(text)
                wordIndices = []
                for word in words:
                    if word in word_dictionary:
                        wordIndices.append(word_dictionary[word])
                    else:
                        print("'%s' not in training corpus; ignoring." % word)
                        untrained_words.append(word)
                return wordIndices

            # reading from saved model
            json_model = open('/Users/stephanie/PycharmProjects/TwitterBot/venv/models/model.json', 'r')
            loaded_model = json_model.read()
            json_model.close()

            model = model_from_json(loaded_model)
            model.load_weights('/Users/stephanie/PycharmProjects/TwitterBot/venv/models/model.h5')

            if len(evalSentence) == 0:
                exit()

            # formatting input
            testArr = convert_text_to_index_array(evalSentence)
            input = tokenizer.sequences_to_matrix([testArr], mode='binary')

            # Classification
            pred = model.predict(input)
            print("%s sentiment; %f%% confidence" % (labels[np.argmax(pred)], pred[0][np.argmax(pred)] * 100))

            pred_dict = {'sentiment':labels[np.argmax(pred)],
                         'confidence':pred[0][np.argmax(pred)] * 100}

            # print("Dumping to json files")
            #
            # with open(
            #         '/Users/stephanie/PycharmProjects/TwitterBot/venv/untrained_corpus/untrained_words_' + str(datetime.now()) + '.json',
            #         'w') as untrained_words_json_file:
            #     json.dump(untrained_words, untrained_words_json_file)


            return  pred_dict