import json
import numpy as np
import tensorflow as tf
import tensorflow.keras.preprocessing.text as kpt
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.models import model_from_json
from datetime import datetime
from sklearn import metrics
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from deep_word_models import ConvNet
from deep_word_models import SimpleLSTM
from deep_word_models import ConvNetLSTM
import pickle


ROWS = None  # Load all reviews (~3.6M)
EPOCHS = 5
VAL_SPLIT = 0.05
BATCH_SIZE = 128
EMBEDDING_DIM = 32
MAX_NUM_WORDS = 30000
MAX_SEQUENCE_LENGTH = 256

EMBEDDING_TYPES = [
    "keras",
    "glove",
    "word2vec"
]

class reviewsentimentanalysis:
    def __init__(self):
        pass
    def classifySentiment(self,evalSentence):
        tokenizer = Tokenizer(num_words=3000)
        labels = ['negative', 'positive']
        untrained_words = []
        with open('/Users/stephanie/PycharmProjects/TwitterBot/venv/models/word_dictionary.json', 'r') as dictionary_file:
            word_dictionary = json.load(dictionary_file)

            def convert_text_to_index_array(text):
                '''
                Checks that all the words in the input
                are registered in the dictionary before
                turning them into matrix
                :param text:
                :return:
                '''
                words = kpt.text_to_word_sequence(text)
                wordIndices = []
                for word in words:
                    if word in word_dictionary:
                        wordIndices.append(word_dictionary[word])
                    else:
                        print("'%s' not in training corpus; ignoring." % word)
                        untrained_words.append(word)
                return wordIndices

            # reading from saved model
            json_model = open('/Users/stephanie/PycharmProjects/TwitterBot/venv/models/model.json', 'r')
            loaded_model = json_model.read()
            json_model.close()

            model = model_from_json(loaded_model)
            model.load_weights('/Users/stephanie/PycharmProjects/TwitterBot/venv/models/model.h5')

            if len(evalSentence) == 0:
                exit()

            # formatting input
            testArr = convert_text_to_index_array(evalSentence)
            input = tokenizer.sequences_to_matrix([testArr], mode='binary')

            # Classification
            pred = model.predict(input)
            print("%s sentiment; %f%% confidence" % (labels[np.argmax(pred)], pred[0][np.argmax(pred)] * 100))

            pred_dict = {'sentiment':labels[np.argmax(pred)],
                         'confidence':pred[0][np.argmax(pred)] * 100}

            print("Dumping to json files")

            with open(
                    '/Users/stephanie/PycharmProjects/TwitterBot/venv/untrained_corpus/untrained_words_' + str(datetime.now()) + '.json',
                    'w') as untrained_words_json_file:
                json.dump(untrained_words, untrained_words_json_file)


            return  pred_dict

    def load_tokenizer(self,tokenizer_path):
        print("\n-- Loading tokenizer")
        with open(tokenizer_path, "rb") as file:
            tokenizer = pickle.load(file)
            return tokenizer

    def reviews_to_sequences(self,reviews, tokenizer, padding="post"):
        print("-- Mapping reviews to sequences\n")
        sequences = tokenizer.texts_to_sequences(reviews)

        return pad_sequences(sequences,
                             maxlen=MAX_SEQUENCE_LENGTH,
                             padding=padding)

    def test(self, model_type, evalReview):
        print("-- Evaluating model")

        tokenizer_path = "/Users/stephanie/PycharmProjects/TwitterBot/venv/models/amazon_review_tokenizer.pkl"

        tokenizer = self.load_tokenizer(tokenizer_path)

        weights_path = "/Users/stephanie/PycharmProjects/TwitterBot/venv/models/amazon_review_convnet.h5"

        model_arguments = {
            "max_words": MAX_NUM_WORDS,
            "max_sequence_length": MAX_SEQUENCE_LENGTH,
            "embedding_dim": EMBEDDING_DIM,
            "weights_path": weights_path
        }

        if model_type== "lstm":
            model = SimpleLSTM(**model_arguments)
        elif model_type=="convnet":
            model = ConvNet(**model_arguments)
        elif model_type=="convnet_lstm":
            model = ConvNetLSTM(**model_arguments)
        else:
            print("\nModel must be specified [--convnet, --lstm, --convnet_lstm]")
            sys.exit(0)

        # test_samples, test_labels = load_and_clean_data(path=TEST_PATH)
        # print("-- Found {} test samples".format(len(test_samples)))

        test_samples = evalReview

        padding = "post" if model_type=="convnet" else "pre"
        test_samples = self.reviews_to_sequences(test_samples, tokenizer, padding)

        predictions = model.predict(test_samples, verbose=1)
        print("Print orig predictions")
        print(predictions)
        predictions = [float(round(x[0])) for x in predictions]

        print("Print predictions")
        print(predictions)

        # accuracy = metrics.accuracy_score(test_labels, predictions)
        # precision = metrics.precision_score(test_labels, predictions)
        # recall = metrics.recall_score(test_labels, predictions)
        # f1_score = metrics.f1_score(test_labels, predictions)
        #
        # print(color_text("Accuracy:  ", color=Fore.GREEN) +
        #       color_text("{:.4f}".format(accuracy), color=Fore.RED))
        #
        # print(color_text("Precision: ", color=Fore.GREEN) +
        #       color_text("{:.4f}".format(precision), color=Fore.RED))
        #
        # print(color_text("Recall:    ", color=Fore.GREEN) +
        #       color_text("{:.4f}".format(recall), color=Fore.RED))
        #
        # print(color_text("F1-score:  ", color=Fore.GREEN) +
        #       color_text("{:.4f}".format(f1_score), color=Fore.RED))


rObject = reviewsentimentanalysis()
rObject.test("convnet","It is a very bad product")