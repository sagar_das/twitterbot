from lxml import html
from json import dump, loads
from requests import get
import json
from re import sub
from time import sleep
from operations.webscrapingutils import XPathScraper
from bs4 import BeautifulSoup
import re

class Scraper:

    url = None
    xpathObj = None
    suffix_url = None
    def __init__(self,url,suffix_url):
        self.url = url
        self.xpathObj = XPathScraper()
        self.suffix_url = suffix_url

    def readResponse(self,url):
        headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36'}
        response = ""
        for i in range(2):
            response = get(url, headers=headers, verify=False, timeout=30)
            if response.status_code == 404:
                return {"url": url, "error": "page not found"}
            if response.status_code != 200:
                continue

        # Removing the null bytes from the response.
        cleaned_response = response.text.replace('\x00', '')

        return cleaned_response

    def parseResponse(self,parser,xpath_list):


        json_details = None

        comments = parser.xpath(xpath_list[2])
        if xpath_list[0] == False:
            cmt_list = []
            for comment in comments:
                comment_text_xpath = xpath_list[3]
                comment_text = comment.xpath(comment_text_xpath)

                try:
                    for cmt in comment_text:
                        cmt = str(cmt).replace(",", "")
                        cmt_list.append(cmt)
                except Exception as e:
                    comment_text = str(comment_text).replace(",", "")
                    cmt_list.append(comment_text)

            json_details = {"comments": cmt_list}

            return json_details


        else:
            #For pre-sentiment classified reviews
            cmt_list = []
            json_details = []
            for comment in comments:
                #Negative comments
                comment_text_xpath = xpath_list[3]
                negative_text = comment.xpath(comment_text_xpath)
                if len(negative_text) == 0:
                    continue
                #Postive comments
                comment_text_xpath = xpath_list[4]
                postive_text = comment.xpath(comment_text_xpath)

                json_details.append({"positive": postive_text,
                                     "negative":negative_text})

            return json_details

    def scrapeComments(self):
        if "page" not in self.url:
            cleaned_response = self.readResponse(self.url)

            parser = html.fromstring(cleaned_response)
            xpath_list = self.xpathObj.findXPathForComments(self.url)

            if "nothing" in xpath_list:
                print("No data found")
                return


            json_details = self.parseResponse(parser,xpath_list)
        else:
            url = self.url+str(1)+self.suffix_url
            cleaned_response = self.readResponse(url)
            soup = BeautifulSoup(cleaned_response, 'html.parser')
            links = soup.find_all('a')

            last_page_number = 0


            #Finding the last page number
            for link in links:
                if "last" in str(link.text).lower():
                    last_page_url = str(link["href"])
                    val = re.findall(r"page=\d+",last_page_url)
                    vals = val[0].split("=")
                    last_page_number = int(vals[1])


            if last_page_number == 0:
                print("No pages found")
                return

            json_details = []
            date_list = []
            for i in range(1,last_page_number+1):
                print("Processing for page "+str(i))
                url = self.url + str(i) + self.suffix_url
                cleaned_response = self.readResponse(url)

                soup = BeautifulSoup(cleaned_response, 'html.parser')
                links = soup.find_all('time')

                for link in links:
                    date_list.append(link.attrs['datetime'])

                parser = html.fromstring(cleaned_response)
                xpath_list = self.xpathObj.findXPathForComments(self.url)

                if "nothing" in xpath_list:
                    print("No data found")
                    return

                json_details.append(self.parseResponse(parser, xpath_list))

                #Suspending the thread for 30 secs
                sleep(30)


        #print(*date_list,sep="\n")
        # with open('/Users/stephanie/PycharmProjects/TwitterBot/venv/data/cpq_data/cpq_' + str(
        #         xpath_list[1]) + '.json', 'w') as json_file:
        #     json.dump(json_details, json_file)

    def scrapeUsers(self):
        if "page" not in self.url:
            pass
        else:
            url = self.url + str(1) + self.suffix_url
            cleaned_response = self.readResponse(url)
            soup = BeautifulSoup(cleaned_response, 'html.parser')
            links = soup.find_all('a')

            last_page_number = 0
            user_id_set = set()
            user_detail_list = []
            # Finding the last page number
            for link in links:
                if "last" in str(link.text).lower():
                    last_page_url = str(link["href"])
                    val = re.findall(r"page=\d+", last_page_url)
                    vals = val[0].split("=")
                    last_page_number = int(vals[1])

            if last_page_number == 0:
                print("No pages found")
                return

            json_details = []
            for i in range(1, last_page_number + 1):
                print("Processing for page " + str(i))
                url = self.url + str(i) + self.suffix_url
                cleaned_response = self.readResponse(url)
                parser = html.fromstring(cleaned_response)

                soup = BeautifulSoup(cleaned_response, 'html.parser')
                links = soup.find_all('a')

                for link in links:
                    try:
                        if "user" in str(link["href"]).lower() and "g2.com" not in str(link["href"]).lower():
                            user_id_set.add(str(link["href"]))
                            print("https://www.g2.com" + str(link["href"]))
                    except Exception as e:
                        continue



                # Suspending the thread for 30 secs
                sleep(30)

            json_list = []
            for user_id in user_id_set:
                url = "https://www.g2.com" + str(user_id)
                cleaned_response = self.readResponse(url)
                soup = BeautifulSoup(cleaned_response, 'html.parser')

                # table = soup.find(lambda tag: tag.name == 'table' and tag.has_attr('class') and tag[
                #     'class'] == "table--vertical-header")
                # rows = table.findAll(lambda tag: tag.name == 'tr')
                script_list = soup.find_all('script', type='application/ld+json')
                for script in script_list:
                    val = script.text
                    json_val = json.loads(val)
                    json_list.append(json_val)

            json_arry = json.dumps(json_list)

            print(json_arry)


        # with open('/Users/stephanie/PycharmProjects/TwitterBot/venv/data/cpq_data/cpq_' + str(
        #         xpath_list[1]) + '.json', 'w') as json_file:
        #     json.dump(json_details, json_file)
