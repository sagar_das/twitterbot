import csv
from nltk import FreqDist
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.metrics import BigramAssocMeasures
from nltk.util import ngrams
from nltk.stem import WordNetLemmatizer
from operations import dboperations as dbo
from collections import Counter
from textblob import TextBlob
from collections import Counter
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from string import punctuation


class tweetanalysis:
    excluded_characters = []
    phrase_counter = None
    def __init__(self):
        self.excluded_characters =[
            ",",
            ".",
            "``",
            "''",
            ";",
            "?",
            "--",
            ")",
            "(",
            ":",
            "!",
            "-",
            "&amp;"
        ]
        self.phrase_counter = Counter()

    def readFromtsv(self,tsvFile):

        with open(tsvFile) as tsvFile:
            reader = csv.reader(tsvFile,delimiter='\t')
            for row in reader:
                max_value = max(row)
                max_val_pos = [i for i, j in enumerate(row) if j == max_value]
                print(round(float(max_value)*100,2))

    def frequencyDistOfText(self,text,hashtag):
        '''

        :param text:
        :return:
        '''
        '''NLTK tokenizer can't be used because it splits # and the word followed by it in a hashtag
        '''
        text_list = text.split(" ")
        for word in text_list:
            if hashtag in word:
                text_list.remove(word)
        freqDist = FreqDist(text_list)
        words = list(freqDist.keys())
        stop_words = set(stopwords.words('english'))
        filtered_sentence = [w for w in text_list if not w in stop_words]

        filtered_sentence = []

        for w in text_list:
            if w not in stop_words:
                filtered_sentence.append(w)

        return filtered_sentence

    def frequentWords(self,text):
        stopwords = set(nltk.corpus.stopwords.words('english'))  # 0(1) lookups
        with_stp = Counter()
        without_stp = Counter()


        spl = text.split()
        # update count off all words in the line that are in stopwrods
        with_stp.update(w.lower().rstrip(punctuation) for w in spl if w.lower() in stopwords)
        # update count off all words in the line that are not in stopwords
        without_stp.update(w.lower().rstrip(punctuation) for w in spl if w not in stopwords)
        # return a list with top ten most common words from each
        return [x for x in with_stp.most_common(10)], [y for y in without_stp.most_common(10)]

    def findRelatedTopics(self,text_list):
        '''

        :param text:
        :return:
        '''
        '''NLTK tokenizer can't be used because it splits # and the word followed by it in a hashtag
        '''

        freqDist = FreqDist(text_list)
        words = list(freqDist.keys())
        stop_words = set(stopwords.words('english'))
        filtered_sentence = [w for w in text_list if not w in stop_words]

        filtered_sentence = []
        frequent_words=[]
        frequent_users=[]
        frequent_hashtags=[]

        for w in text_list:
            if w not in stop_words:
                filtered_sentence.append(w)

        #Getting the list of most frequently used words
        common_words = freqDist.most_common()

        for word in common_words:
            if '#' in word[0]:
                frequent_hashtags.append(word)
            elif '@' in word[0]:
                frequent_users.append(word)
            else:
                frequent_words.append(word)


        # print('Hash tags')
        # print(frequent_hashtags)
        # print('Users')
        # print(frequent_users)
        # print('Words')
        # print(frequent_words)

        return frequent_hashtags,frequent_users,frequent_words


        #PLotting the most frequently used words
        #freqDist.plot(10)

    def findCollocations(self,corpus):

        n = 25

        all_tokens = [token for doc in corpus for token in str(doc).lower().split()]

        finder = nltk.BigramCollocationFinder.from_words(all_tokens)
        finder.apply_freq_filter(2)
        finder.apply_word_filter(lambda w: w in nltk.corpus.stopwords.words('english'))
        scorer = BigramAssocMeasures.jaccard
        collocations = finder.nbest(scorer,n )

        for collocation in collocations:
            c = ' '.join(collocation)
            print(c)

    def frequentNgrams(self,text,n_gram_length):
        stopwords = set(nltk.corpus.stopwords.words('english'))
        if n_gram_length is None:
            n_gram_length=2

        for char in self.excluded_characters:
            text = text.replace(char,"")



        for sentence in nltk.sent_tokenize(text):
            words = nltk.word_tokenize(sentence)
            for phrase in ngrams(words,n_gram_length):
                self.phrase_counter[phrase] +=1

    def findFrequentNgrams(self,text_list):
        # for text in text_list:
        self.frequentNgrams(" ".join(text_list), 5)

        most_common_phrases = self.phrase_counter.most_common(50)
        for k, v in most_common_phrases:
            print('{0: <5}'.format(v), k)

    def findNouns(self,sentence):

        blob = TextBlob(sentence)
        return blob.noun_phrases

    def findProperNouns(self,sentence):

        proper_nouns=[]

        for word, pos in nltk.pos_tag(nltk.word_tokenize(sentence)):
            if (pos == 'NNP'):
                proper_nouns.append(word)
        return proper_nouns

    def findAdjectives(self,sentence):

        adjectives=[]

        for word, pos in nltk.pos_tag(nltk.word_tokenize(sentence)):
            if (pos == 'JJ' or pos=="JJR" or pos=="JJS"):
                adjectives.append(word)
        return adjectives

    #region Text similarity

    def lemmatize_word(self,word,pos):
        """

        :param word:
        :param pos: "v" for verb, "n" for noun
        :return:
        """
        lemmatizer = WordNetLemmatizer()
        return lemmatizer.lemmatize(word,pos=pos)

    def get_jaccard_similarity(self,str1, str2):

        str1_words = nltk.word_tokenize(str1)
        str2_words = nltk.word_tokenize(str2)

        sen1 =""
        sen2 =""

        for word in str1_words:
            sen1+= str(self.lemmatize_word(word,"v")) + " "

        for word in str2_words:
            sen2+= str(self.lemmatize_word(word,"v")) + " "


        a = set(sen1.split())
        b = set(sen2.split())
        c = a.intersection(b)
        return float(len(c)) / (len(a) + len(b) - len(c))

    def get_vectors(self,sentences):
        text = [t for t in sentences]
        vectorizer = CountVectorizer(text)
        vectorizer.fit(text)
        return vectorizer.transform(text).toarray()

    def get_cosine_similarity(self,str1,str2):

        str1_words = nltk.word_tokenize(str1)
        str2_words = nltk.word_tokenize(str2)

        sen1 = ""
        sen2 = ""

        for word in str1_words:
            sen1 += str(self.lemmatize_word(word, "v")) + " "

        for word in str2_words:
            sen2 += str(self.lemmatize_word(word, "v")) + " "


        sentences = [sen1,sen2]

        vectors = [t for t in self.get_vectors(sentences)]
        sim = cosine_similarity(vectors)
        return sim


    #endregion

















# ta = tweetanalysis()
# #text = '#Blue #Blue #Blue #Blue #Blue Connect with your constituents like never before and become a Connected Campus and School with Salesforce Education Cloud!'
# #education #salesforce https://t.co/ceuiB0mrhD'
#
# #education #salesforce https://t.co/ceuiB0mrhD'
# #ta.readFromtsv('/Users/stephanie/PycharmProjects/TwitterBot/venv/models/bert_models/test_results.tsv')
# db = dbo.dboperations()
# result = db.selectRawQuery("select distinct * from tweets where hash_tag = 'salesforce' and tweet_text not like 'RT%'")
#
# final_word_list = []
# final_text_list = ""
# for row in result:
#     final_word_list += ta.frequencyDistOfText(str(row[1]).lower(),'salesforce')
#     final_text_list += str(row[1]) +"."
#
# # ta.findRelatedTopics(final_word_list)
# #ta.findCollocations(final_word_list)
# ta.frequentNgrams(final_text_list,3)


