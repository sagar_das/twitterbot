import re
class XPathScraper:
    def __init__(self):
        pass

    def findXPathForComments(self,website):

        dot_occurences = [i for i, a in enumerate(website) if a == "."]
        website_name = str(website[dot_occurences[0]+1:dot_occurences[1]]).lower()


        """
        Format -  Boolean value for pre-sentiment-classified text, Website_name, Comment Xpath, Xpath attribute(s)
        
        If pre-sentiment-classified text is true
        Xpath attribute[0] - Negative
        Xpath attribute[1] - Positive
        """
        switcher ={
            "trustradius": [False,website_name,'//div[contains(@class,"ugc")]','.//text()'],
            "g2": [True,website_name,'//div[contains(@class,"f-1")]','.//div[contains(@itemprop,"reviewBody")]//'+
                                                                      'div[position()=2]//p[contains(@class,"formatted-text")]//text()',
                   './/div[contains(@itemprop,"reviewBody")]//' +
                   'div[position()=1]//p[contains(@class,"formatted-text")]//text()'
                   ]
        }

        return switcher.get(website_name,"nothing")

    def findXPathForContents(self, website):

        dot_occurences = [i for i, a in enumerate(website) if a == "."]
        website_name = str(website[dot_occurences[0] + 1:dot_occurences[1]]).lower()

        switcher = {
            0: "zero",
            1: "one"
        }

        return switcher.get(website_name, "nothing")

    def findXPathForUsers(self, website):

        dot_occurences = [i for i, a in enumerate(website) if a == "."]
        website_name = str(website[dot_occurences[0] + 1:dot_occurences[1]]).lower()

        switcher = {
            "trustradius": ['//div[contains(@class,"ugc")]', './/text()'],
            "g2": ['//div[contains(@class,"paper")]//div[contains(@class,"grid-x grid-margin-x")]'+
                   '//div[contains(@class,"cell large-auto")]//div[contains(@class,"pjax-container pjax-short-loader")]'+
                   '//div[contains(@id,"profile_details")]//div[contains(@class,"grid-x grid-margin-x grid-margin-y")]'+
                   '//div[contains(@id,"cell xlarge-6")]//table[contains(@class,"table--vertical-header")]']
        }

        return switcher.get(website_name, "nothing")