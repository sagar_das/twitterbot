import urllib3 as ulib
from bs4 import BeautifulSoup
from messages import Messages
from MailMan import sendEmail
import tornado.ioloop
from tornado.httpclient import AsyncHTTPClient
from tornado import gen
import logging

linksBody =[]
soup = None

#Initializing log operations
logging.basicConfig(filename='cxmarketbot.log', filemode='a', format='%(name)s - %(levelname)s - %(message)s')


httpclient = AsyncHTTPClient()

def handleResponse(response):
    global soup
    if response.error :
        print('Error in fetching response')
        logging.error('Error occured in fetching response from URL  '+str(response
                                                                          .request.url))
    else:
        data = response.body
        soup = BeautifulSoup(data,'html.parser')
        parseSoupLinks(soup)




def getPage():
    poolManager = ulib.PoolManager()
    response = poolManager.request('GET',Messages.cx_website)
    return response.data

def parseSoupLinks(soup,url):
    for links in soup.findAll('a',href=True):
        if 'cmswire' in  url:
            if 'market' in links['href']:
                linksBody.append(Messages.cms_website+links['href'])
                print(Messages.cms_website+links['href'])
        elif 'commercecx' in url:
            if 'blog' in links['href']:
                linksBody.append(links['href'])
                print(links['href'])


@gen.coroutine
def fetchFromUrl(url):
    global httpclient
    response = yield httpclient.fetch(url)
    print(response.body)
    soup = BeautifulSoup(response.body, 'html.parser')
    parseSoupLinks(soup,url)



page = getPage()
soup = BeautifulSoup(page,'html.parser')
parseSoupLinks(soup,Messages.cx_website)
sendEmail(linksBody[5])

#httpclient.fetch(Messages.cms_website,handleResponse)
# try:
#     fetchFromUrl(Messages.cx_website)
# except Exception as e:
#     logging.exception(e)
# tornado.ioloop.IOLoop.instance().start()


